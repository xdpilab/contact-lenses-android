package ru.xdpi.contactlenses.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.model.Recipe;

public class RecipeAdapter extends ArrayAdapter<Recipe> {
    private Context mContext;
    private List<Recipe> mRecipes;

    public RecipeAdapter(Context context, List<Recipe> recipes) {
        super(context, 0, recipes);
        mContext = context;
        mRecipes = recipes;
    }

    @Override
    public int getCount() {
        return mRecipes.size();
    }

    @Override
    public Recipe getItem(int position) {
        return mRecipes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mRecipes.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            view = inflater.inflate(R.layout.adapter_recipe_list, parent, false);

            final ViewHolder holder = new ViewHolder();
            holder.mRecipeName = view.findViewById(R.id.recipe_name);
            view.setTag(holder);

        } else {
            view = convertView;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.mRecipeName.setText(mRecipes.get(position).getRecipeName());

        return view;
    }

    private static class ViewHolder {
        TextView mRecipeName;
    }

}
