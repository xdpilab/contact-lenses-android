package ru.xdpi.contactlenses.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.model.Info;

public class UsefulInfoAdapter extends ArrayAdapter<Info> {
    private Context mContext;
    private List<Info> mInfoItems;

    public UsefulInfoAdapter(Context context, List<Info> infoItems) {
        super(context, 0, infoItems);
        mContext = context;
        mInfoItems = infoItems;
    }

    @Override
    public int getCount() {
        return mInfoItems.size();
    }

    @Override
    public Info getItem(int position) {
        return mInfoItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mInfoItems.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            view = inflater.inflate(R.layout.adapter_useful_info, parent, false);

            final ViewHolder holder = new ViewHolder();

            holder.mLink = view.findViewById(R.id.useful_info_link);
            holder.mDescription = view.findViewById(R.id.useful_info_description);

            view.setTag(holder);

        } else
            view = convertView;

        ViewHolder holder = (ViewHolder) view.getTag();
        holder.mLink.setText(mInfoItems.get(position).getLink());
        holder.mDescription.setText(mInfoItems.get(position).getDescription());

        return view;
    }

    private static class ViewHolder {
        TextView mLink;
        TextView mDescription;
    }

}