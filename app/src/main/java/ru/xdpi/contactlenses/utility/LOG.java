package ru.xdpi.contactlenses.utility;

import android.util.Log;

public final class LOG {
    public static String TAG = "ru.xdpi.calculator";

    public static void d(String msg) {
        d(null, msg);
    }

    public static void d(String tag, String msg) {
        Log.d(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), msg);
    }

    public static void d(String tag, int msg) {
        Log.d(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), String.valueOf(msg));
    }

    public static void i(String tag, String msg) {
        Log.i(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), msg);
    }

    public static void i(String tag, int msg) {
        Log.d(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), String.valueOf(msg));
    }

    public static void e(String msg) {
        e(null, msg);
    }

    public static void e(String tag, String msg) {
        Log.e(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), msg);
    }

    public static void e(String tag, int msg) {
        Log.d(TAG + (tag != null && !tag.equals("") ? "." + tag : ""), String.valueOf(msg));
    }

}