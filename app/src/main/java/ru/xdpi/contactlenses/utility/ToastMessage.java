package ru.xdpi.contactlenses.utility;

import android.content.Context;
import android.widget.Toast;

public final class ToastMessage {

    public static void toastL(Context c, String message) {
        Toast.makeText(c, message, Toast.LENGTH_LONG).show();
    }

    public static void toastS(Context c, String message) {
        Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
    }

    public static void toastL(Context c, int value, boolean isFromResources) {
        if (isFromResources)
            Toast.makeText(c, c.getResources().getText(value), Toast.LENGTH_LONG).show();
        else
            Toast.makeText(c, value, Toast.LENGTH_LONG).show();
    }

    public static void toastS(Context c, int value, boolean isFromResources) {
        if (isFromResources)
            Toast.makeText(c, c.getResources().getText(value), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(c, value, Toast.LENGTH_SHORT).show();
    }

}