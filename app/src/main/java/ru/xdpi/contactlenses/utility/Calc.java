package ru.xdpi.contactlenses.utility;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class Calc {
    public long mDiff;

    public long getDiff() {
        return mDiff;
    }

    public void setDiff(long diff) {
        mDiff = diff;
    }

    public int doCalc(DateTime end, DateTime start) {
        mDiff = Days.daysBetween(start.withTimeAtStartOfDay(), end.withTimeAtStartOfDay()).getDays();
        return (int) mDiff;
    }

}
