package ru.xdpi.contactlenses.utility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class Utility {

    public static boolean hasCharacter(String string, char c) {
        return string.indexOf(c) > -1;
    }

    public static void setCursorToEnd(EditText editText) {
        editText.setSelection(editText.getText().length());
    }

    private void statusBarVisibilityOld(Activity context, boolean isShow) {
        if (isShow)
            context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        else
            context.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
    }

    private void statusBarVisibility(Activity context, boolean isShow) {
        if (Build.VERSION.SDK_INT < 16)
            statusBarVisibilityOld(context, isShow);
        else {
            View decorView = context.getWindow().getDecorView();
            int uiOptions = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                if (isShow)
                    uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
                else
                    uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public static void hideKeyboard(Activity context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View focus = context.getCurrentFocus();
        if (focus != null)
            inputMethodManager.hideSoftInputFromWindow(focus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static boolean isPackageInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}