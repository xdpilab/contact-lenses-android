package ru.xdpi.contactlenses.utility;

import android.content.SharedPreferences;
import android.widget.LinearLayout;

import ru.xdpi.contactlenses.R;

public class PanelColor {
    public static final String PANEL_COLOR = "panel_color";

    public void setBgFragment(SharedPreferences sp, String key, LinearLayout l) {
        switch (key) {
            case "0":
                l.setBackgroundResource(R.drawable.gradient_1);
                break;

            case "1":
                l.setBackgroundResource(R.drawable.gradient_2);
                break;

            case "2":
                l.setBackgroundResource(R.drawable.gradient_3);
                break;

            case "3":
                l.setBackgroundResource(R.drawable.gradient_4);
                break;

            case "4":
                l.setBackgroundResource(R.drawable.gradient_5);
                break;

            case "5":
                l.setBackgroundResource(R.drawable.gradient_6);
                break;

            case "6":
                l.setBackgroundResource(R.drawable.gradient_7);
                break;
        }
    }

    public void setBgFragment(SharedPreferences sp, String key, LinearLayout l, LinearLayout l1) {
        switch (key) {
            case "0":
                l.setBackgroundResource(R.drawable.gradient_1);
                l1.setBackgroundResource(R.drawable.gradient_1);
                break;

            case "1":
                l.setBackgroundResource(R.drawable.gradient_2);
                l1.setBackgroundResource(R.drawable.gradient_2);
                break;

            case "2":
                l.setBackgroundResource(R.drawable.gradient_3);
                l1.setBackgroundResource(R.drawable.gradient_3);
                break;

            case "3":
                l.setBackgroundResource(R.drawable.gradient_4);
                l1.setBackgroundResource(R.drawable.gradient_4);
                break;

            case "4":
                l.setBackgroundResource(R.drawable.gradient_5);
                l1.setBackgroundResource(R.drawable.gradient_5);
                break;

            case "5":
                l.setBackgroundResource(R.drawable.gradient_6);
                l1.setBackgroundResource(R.drawable.gradient_6);
                break;

            case "6":
                l.setBackgroundResource(R.drawable.gradient_7);
                l1.setBackgroundResource(R.drawable.gradient_7);
                break;
        }
    }

}