package ru.xdpi.contactlenses

object Constant {

    // Google Play
    val URL_MARKET_XDPI_LAB = "market://search?q=pub:XDPI Lab"
    val URL_MARKET_CURIOUS_BUNNY = "market://details?id=ru.xdpi.curiousbunny"
    val URL_MARKET_CURIOUS_BUNNY_FREE = "market://details?id=ru.xdpi.curiousbunnyfree"
    val URL_MARKET_CONTACT_LENSES = "market://details?id=ru.xdpi.contactlenses"
    val URL_MARKET_CONTACT_LENSES_PRO = "market://details?id=ru.xdpi.contactlensespro"
    val URL_MARKET_CALCULATOR = "market://details?id=ru.xdpi.calculator"
    val URL_MARKET_STOPWATCH = "market://details?id=ru.xdpi.stopwatch"

    val PACKAGE_STOPWATCH = "ru.xdpi.stopwatch"
    val PACKAGE_CALCULATOR = "ru.xdpi.calculator"

    val E_MAIL = "xdpilab@gmail.com"
    val FACEBOOK_LIKE = "https://www.facebook.com/xdpi.contactlenses"

    // Wiki articles links
    val WIKI_EN = "https://en.wikipedia.org/wiki/Contact_lens"
    val WIKI_DE = "https://de.wikipedia.org/wiki/Kontaktlinse"
    val WIKI_ES = "https://es.wikipedia.org/wiki/Lente_de_contacto"
    val WIKI_FR = "https://fr.wikipedia.org/wiki/Lentilles_de_contact"
    val WIKI_IT = "https://it.wikipedia.org/wiki/Lente_a_contatto"
    val WIKI_JP = "https://ja.wikipedia.org/wiki/コンタクトレンズ"
    val WIKI_RU = "https://ru.wikipedia.org/wiki/Контактные_линзы"

    // Extra
    val TEXT_PLAIN = "text/plain"
    val TEXT_HTML = "text/html"
    val yyyy_MM_dd = "yyyy/MM/dd"

    val MAPS_PERMISSION_HELP = "https://support.google.com/accounts/answer/6179507"

}