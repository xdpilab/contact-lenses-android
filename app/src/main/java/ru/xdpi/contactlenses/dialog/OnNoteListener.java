package ru.xdpi.contactlenses.dialog;

public interface OnNoteListener {
    void onGetNoteText(String inputText);
}
