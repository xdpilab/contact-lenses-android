package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.Context;

import ru.xdpi.contactlenses.R;

public class Alert extends AlertDialog.Builder {
    public Alert(Context context, String message) {
        super(context);
        setMessage(message);
        setCancelable(false);
        setPositiveButton(R.string.ok, null);
    }

}
