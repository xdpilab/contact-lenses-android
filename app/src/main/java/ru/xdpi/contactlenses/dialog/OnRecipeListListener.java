package ru.xdpi.contactlenses.dialog;

public interface OnRecipeListListener {
    void onGetRecipeList(int inputText);
}
