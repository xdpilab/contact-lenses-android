package ru.xdpi.contactlenses.dialog;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.model.Info;
import ru.xdpi.contactlenses.utility.LOG;
import ru.xdpi.contactlenses.utility.PanelColor;
import ru.xdpi.contactlenses.utility.ToastMessage;

public class AddInfo extends DialogFragment implements OnClickListener, OnSharedPreferenceChangeListener {
    public static final String TAG = "dialog." + AddInfo.class.getSimpleName();
    public static final String OPEN = "open_add_info_dialog";
    private OnAddInfoListener onAddInfoListener;
    private EditText mEditLink;
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private EditText mEditDescription;

    public AddInfo() {

    }

    public void setOnAddInfoListener(OnAddInfoListener listener) {
        onAddInfoListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_dialog_add_info, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        // auto keyboard
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mLayout = v.findViewById(R.id.layout_left);
        TextView textEye = v.findViewById(R.id.textView);
        mEditLink = v.findViewById(R.id.link_edit);
        mEditDescription = v.findViewById(R.id.description_edit);
        Button btnOk = v.findViewById(R.id.button1);
        Button btnCancel = v.findViewById(R.id.button2);

        textEye.setText(R.string.dialog_add_info);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                if (mEditLink.length() != 0 && mEditDescription.length() != 0) {
                    onAddInfoListener.onGetAddInfoText(mEditLink.getText().toString(),
                            mEditDescription.getText().toString());
                    Info info = new Info(mEditLink.getText().toString(), mEditDescription.getText().toString());
                    try {
                        List<Info> infoItems = DatabaseFactory.getHelper().getUsefulInfoDAO().getAll();
                        if (!infoItems.isEmpty()) {
                            DatabaseFactory.getHelper().getUsefulInfoDAO().create(info);
                            LOG.d(TAG, "infoItems - not empty");
                        } else {
                            DatabaseFactory.getHelper().getUsefulInfoDAO().create(info);
                            LOG.d(TAG, "infoItems - empty");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    dismiss();

                } else {
                    ToastMessage.toastL(getActivity(), R.string.toast_info_fields_empty, true);
                }
                break;

            case R.id.button2:
                dismiss();
                break;

            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLayout);
    }

}
