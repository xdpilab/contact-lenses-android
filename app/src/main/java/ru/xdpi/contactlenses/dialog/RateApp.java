package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;

import java.util.Date;

import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.ConstantFlavor;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.Device;
import ru.xdpi.contactlenses.utility.LOG;
import ru.xdpi.contactlenses.utility.Utility;

public class RateApp {
    private static final String TAG = "dialog." + RateApp.class.getSimpleName();
    private static final String PREF_NAME = "RateThisApp";
    private static final String KEY_INSTALL_DATE = "rta_install_date";
    private static final String KEY_LAUNCH_TIMES = "rta_launch_times";
    private static final String KEY_OPT_OUT = "rta_opt_out";

    private static Date mInstallDate = new Date();
    private static int mLaunchTimes = 0;
    private static boolean mOptOut = false;

    // days after installation until showing rate dialog
    public static final int INSTALL_DAYS = 14;

    // app launching times until showing rate dialog
    public static final int LAUNCH_TIMES = 25;


    // call this API when the launcher activity is launched. It is better to
    // call this API in onStart() of the launcher activity.
    public static void onStart(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = pref.edit();

        // if it is the first launch, save the date in shared preference.
        if (pref.getLong(KEY_INSTALL_DATE, 0) == 0L) {
            Date now = new Date();
            editor.putLong(KEY_INSTALL_DATE, now.getTime());
            LOG.d(TAG, "First install: " + now.toString());
        }

        // increment launch times
        int launchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0);
        launchTimes++;
        editor.putInt(KEY_LAUNCH_TIMES, launchTimes);
        LOG.d(TAG, "Launch times; " + launchTimes);

        editor.apply();

        mInstallDate = new Date(pref.getLong(KEY_INSTALL_DATE, 0));
        mLaunchTimes = pref.getInt(KEY_LAUNCH_TIMES, 0);
        mOptOut = pref.getBoolean(KEY_OPT_OUT, false);

        printStatus(context);
    }

    // show the rate dialog if the criteria is satisfied
    public static void showRateDialogIfNeeded(final Context context) {
        if (shouldShowRateDialog()) {
            showRateDialog(context);
        }
    }

    // check whether the rate dialog should be shown or not
    private static boolean shouldShowRateDialog() {
        if (mOptOut)
            return false;
        else {
            if (mLaunchTimes >= LAUNCH_TIMES) {
                return true;
            }
            int threshold = INSTALL_DAYS * 24 * 60 * 60 * 1000; //  milliseconds
            return new Date().getTime() - mInstallDate.getTime() >= threshold;
        }
    }

    public static void showRateDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(R.string.dialog_rta_title);
        builder.setMessage(R.string.dialog_rta_message);
        builder.setPositiveButton(R.string.dialog_rta_ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(ConstantFlavor.CONTACT_LENSES));
                context.startActivity(i);
                setOptOut(context, true);
            }
        });

        builder.setNeutralButton(R.string.dialog_rta_cancel, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearSharedPreferences(context);
            }
        });

        builder.setNegativeButton(R.string.dialog_rta_no, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setOptOut(context, true);
                Intent send = new Intent(Intent.ACTION_SEND);
                send.setType(Constant.INSTANCE.getTEXT_HTML());
                send.putExtra(Intent.EXTRA_EMAIL, new String[]{Constant.INSTANCE.getE_MAIL()});
                send.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.debug_info) + context.getString(R.string.app_name));
                send.putExtra(Intent.EXTRA_TEXT, "\n" + "\n" + "\n" +
                        "\n" + context.getString(R.string.symbol_dash) +
                        "\n" + context.getString(R.string.do_not_remove) +
                        "\n" + context.getString(R.string.symbol_dash) +
                        "\n" + context.getString(R.string.android_sdk) + Device.getAndroidVersion() +
                        "\n" + context.getString(R.string.app_version) + Device.getVersionName(context) +
                        "\n" + context.getString(R.string.device) + Device.getDeviceName() +
                        "\n" + context.getString(R.string.country) + Device.getCountry(context) +
                        "\n" + context.getString(R.string.language) + Device.getLanguage());
                try {
                    context.startActivity(Intent.createChooser(send, context.getString(R.string.menu_send_recipe)));
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                clearSharedPreferences(context);
            }
        });

        builder.create().show();
    }

    // clear data in shared pref_main. This API is called when the rate dialog is approved or canceled
    private static void clearSharedPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.remove(KEY_INSTALL_DATE);
        editor.remove(KEY_LAUNCH_TIMES);
        editor.apply();
    }

    // set opt out flag. If it is true, the rate dialog will never shown unless app data is cleared
    private static void setOptOut(final Context context, boolean optOut) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        Editor editor = pref.edit();
        editor.putBoolean(KEY_OPT_OUT, optOut);
        editor.apply();
    }

    // print values in SharedPreferences (used for debug)
    private static void printStatus(final Context context) {
        SharedPreferences pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        LOG.d(TAG, "*** RateThisApp Status ***");
        LOG.d(TAG, "Install Date: " + new Date(pref.getLong(KEY_INSTALL_DATE, 0)));
        LOG.d(TAG, "Launch Times: " + pref.getInt(KEY_LAUNCH_TIMES, 0));
        LOG.d(TAG, "Opt out: " + pref.getBoolean(KEY_OPT_OUT, false));
    }

}
