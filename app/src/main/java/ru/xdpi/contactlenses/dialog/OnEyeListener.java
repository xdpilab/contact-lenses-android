package ru.xdpi.contactlenses.dialog;

public interface OnEyeListener {
    void onGetLeftText(int inputText);

    void onGetRightText(int inputText);
}
