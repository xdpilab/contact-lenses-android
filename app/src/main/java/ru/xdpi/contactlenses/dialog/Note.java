package ru.xdpi.contactlenses.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.PanelColor;

public class Note extends DialogFragment implements OnClickListener, OnSharedPreferenceChangeListener {
    public static final String TAG = "dialog." + Note.class.getSimpleName();
    public static final String OPEN = "open_note_dialog";
    public static final String NOTE_SAVED = "note_saved_text";
    private OnNoteListener mOnNoteListener;
    private EditText mEditText;
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private Editor mEd;

    public Note() {

    }

    public void setOnNoteListener(OnNoteListener listener) {
        mOnNoteListener = listener;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
        mEd = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_dialog_note, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        // auto keyboard
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mLayout = view.findViewById(R.id.layout_note);
        mEditText = view.findViewById(R.id.txt_edit);
        Button btnOk = view.findViewById(R.id.button1);
        Button btnCancel = view.findViewById(R.id.button2);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        loadEditText();

        setAppTheme(mSp);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                try {
                    mOnNoteListener.onGetNoteText(mEditText.getText().toString());
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                saveEditText();
                dismiss();
                break;

            case R.id.button2:
                dismiss();
                break;

            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    void saveEditText() {
        mEd.putString(NOTE_SAVED, mEditText.getText().toString());
        mEd.apply();
    }

    void loadEditText() {
        mEditText.setText(mSp.getString(NOTE_SAVED, ""));

        // cursor to end
        mEditText.setSelection(mEditText.getText().length());
    }

    private void setAppTheme(SharedPreferences sp) {
        PanelColor color = new PanelColor();
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        color.setBgFragment(sp, key, mLayout);
    }

}
