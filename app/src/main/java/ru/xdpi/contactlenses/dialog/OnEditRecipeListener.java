package ru.xdpi.contactlenses.dialog;

public interface OnEditRecipeListener {
    void onGetEditRecipeText(
            String recipeName, String lenses, String days,
            String leftSPH, String rightSPH,
            String leftCYL, String rightCYL,
            String leftAX, String rightAX,
            String leftBC, String rightBC,
            String leftDIA, String rightDIA,
            String leftADD, String rightADD,
            String extra, String extra1);
}
