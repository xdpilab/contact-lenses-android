package ru.xdpi.contactlenses.dialog;

public interface OnAddInfoListener {
    void onGetAddInfoText(String link, String description);
}
