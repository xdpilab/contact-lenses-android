package ru.xdpi.contactlenses.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.Utility;

public class PrefNotifyText extends DialogPreference {
    public static final String TAG = "dialog." + PrefNotifyText.class.getSimpleName();
    public static final String NOTIFY_TEXT = "sp_notify_text";
    private static final int LAYOUT = R.layout.fr_dialog_pref_notif_text;
    private final SharedPreferences mSp;
    private Editor mEd;
    private EditText mNotifyText;

    @SuppressLint("CommitPrefEdits")
    public PrefNotifyText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(LAYOUT);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        mSp = PreferenceManager.getDefaultSharedPreferences(context);
        mEd = PreferenceManager.getDefaultSharedPreferences(context).edit();

        if ("".equals(mSp.getString(NOTIFY_TEXT, "")))
            setSummary(R.string.notify_title);
        else
            setSummary(mSp.getString(NOTIFY_TEXT, ""));
    }

    @Override
    protected void onBindDialogView(@NonNull View view) {
        mNotifyText = view.findViewById(R.id.edit_notify_text);

        if ("".equals(mSp.getString(NOTIFY_TEXT, "")))
            mNotifyText.setText(R.string.notify_title);
        else
            mNotifyText.setText(mSp.getString(NOTIFY_TEXT, ""));

        Utility.setCursorToEnd(mNotifyText);
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            mEd.putString(NOTIFY_TEXT, mNotifyText.getText().toString());
            mEd.apply();

            setSummary(mNotifyText.getText().toString());
        }
    }

}
