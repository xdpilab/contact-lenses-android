package ru.xdpi.contactlenses.dialog;

import android.annotation.SuppressLint;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.xdpi.contactlenses.ConstantFlavor;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.PanelColor;

public class Eye extends DialogFragment implements OnClickListener, OnSharedPreferenceChangeListener {
    public static final String TAG = "dialog." + Eye.class.getSimpleName();
    public static final String OPEN_LEFT = "open_left_dialog";
    public static final String OPEN_RIGHT = "open_right_dialog";
    public static final String EYE_TEXT = "eye_text";
    public static final String EYE_TEXT_INT = "eye_text_int";
    private final String LEFT_SAVED = "left_saved_text";
    private final String RIGHT_SAVED = "right_saved_text";
    private OnEyeListener mOnEyeListener;
    private EditText mEditText;
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private Editor mEd;
    private String mEyeText;
    private int mLeftOrRight;

    public Eye() {

    }

    public void setOnLeftRightListener(OnEyeListener listener) {
        mOnEyeListener = listener;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
        mEd = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();

        mEyeText = getArguments().getString(EYE_TEXT);
        mLeftOrRight = getArguments().getInt(EYE_TEXT_INT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_dialog_eye, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        // auto keyboard
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mLayout = v.findViewById(R.id.layout_left);
        TextView textEye = v.findViewById(R.id.textView1);
        mEditText = v.findViewById(R.id.txt_edit);
        final Button btnOk = v.findViewById(R.id.button1);
        Button btnCancel = v.findViewById(R.id.button2);

        textEye.setText(mEyeText);

        btnOk.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mEditText == null || mEditText.length() == 0) {
                    btnOk.setEnabled(false);
                } else {
                    btnOk.setEnabled(true);
                }

                try {
                    int val = Integer.parseInt(mEditText.getText().toString());
                    if (val < 1)
                        mEditText.setText("");
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        });

        loadEditText();

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                try {
                    int val = Integer.parseInt(mEditText.getText().toString());
                    if (val > ConstantFlavor.LIMIT_DAYS) {
                        new AlertPeriod(getActivity()).create().show();
                    } else {
                        try {
                            if (mLeftOrRight == 0) {
                                mOnEyeListener.onGetLeftText(Integer.parseInt(mEditText.getText().toString()));
                            } else if (mLeftOrRight == 1) {
                                mOnEyeListener.onGetRightText(Integer.parseInt(mEditText.getText().toString()));
                            }
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        saveEditText();
                        dismiss();
                    }
                } catch (NumberFormatException ex) {
                    ex.printStackTrace();
                }
                break;

            case R.id.button2:
                dismiss();
                break;

            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    private void saveEditText() {
        if (mLeftOrRight == 0) {
            mEd.putString(LEFT_SAVED, mEditText.getText().toString());
        } else if (mLeftOrRight == 1) {
            mEd.putString(RIGHT_SAVED, mEditText.getText().toString());
        }
        mEd.apply();
    }

    private void loadEditText() {
        if (mLeftOrRight == 0) {
            mEditText.setText(mSp.getString(LEFT_SAVED, ""));
        } else if (mLeftOrRight == 1) {
            mEditText.setText(mSp.getString(RIGHT_SAVED, ""));
        }
        // cursor to the end
        mEditText.setSelection(mEditText.getText().length());
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLayout);
    }

}
