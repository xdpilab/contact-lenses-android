package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.Context;

import ru.xdpi.contactlenses.R;

public class NoConnection extends AlertDialog.Builder {
    public NoConnection(final Context context) {
        super(context);
        setMessage(R.string.dialog_connection);
        setCancelable(false);
        setPositiveButton(R.string.ok, null);
    }

}
