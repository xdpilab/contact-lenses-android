package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.adapter.RecipeAdapter;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.model.Recipe;
import ru.xdpi.contactlenses.utility.PanelColor;

public class RecipeList extends DialogFragment implements OnSharedPreferenceChangeListener {
    public static final String TAG = "dialog." + RecipeList.class.getSimpleName();
    public static final String OPEN = "open_recipe_list_dialog";
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private OnRecipeListListener mOnRecipeListListener;

    public RecipeList() {

    }

    public void setOnRecipeListListener(OnRecipeListListener listener) {
        mOnRecipeListListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_dialog_recipe_list, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        mLayout = v.findViewById(R.id.layout_recipes_list);
        final ListView listView = v.findViewById(R.id.listView);

        try {
            final List<Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
            final RecipeAdapter adapter = new RecipeAdapter(getActivity(), recipes);
            listView.setAdapter(adapter);

            listView.setOnItemClickListener((parent, view, position, id) -> {
                mOnRecipeListListener.onGetRecipeList(position);
                dismiss();
//                    LOG.d(TAG, "onItemClick " + position);
            });

            listView.setOnItemLongClickListener((parent, view, position, id) -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.dialog_message_delete_recipe);
                builder.setPositiveButton(R.string.ok, (dialog, id1) -> {
//                        LOG.d(TAG, "position " + position);
                    Recipe recipe = recipes.get(position);
                    recipes.remove(recipe);

                    // bad solution to create each time new adapter, but notifyDataSetChanged() - doesn't work properly !!!
                    listView.setAdapter(new RecipeAdapter(getActivity(), recipes));

                    try {
                        DatabaseFactory.getHelper().getRecipeDAO().deleteRowByRecipeName(recipe.getRecipeName());
                        mOnRecipeListListener.onGetRecipeList(position - 1);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
                builder.setNegativeButton(R.string.cancel, null);
                AlertDialog dialog = builder.create();
                dialog.show();

                return true;
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLayout);
    }

}