package ru.xdpi.contactlenses.dialog;

import android.content.Context;

public class DialogController {

    public static void showAlert(Context c, String message) {
        new Alert(c, message).create().show();
    }

    public static void showNoConnection(Context c) {
        new NoConnection(c).create().show();
    }

    public static void showNoGps(Context c) {
        new NoGps(c).create().show();
    }

    public static void showRateApp(Context c) {
        RateApp.showRateDialog(c);
    }

}