package ru.xdpi.contactlenses.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

public class PrefTimePicker extends DialogPreference {
    private static final String HOUR = ".hour";
    private static final String MINUTE = ".minute";
    private static final String FORMAT = "%d:%02d";
    public static final int DEFAULT_HOUR = 12, DEFAULT_MINUTE = 0;
    private android.widget.TimePicker mTimePicker;

    @SuppressLint("DefaultLocale")
    public PrefTimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);

        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        int hour = sp.getInt(getKey() + HOUR, DEFAULT_HOUR);
        int minute = sp.getInt(getKey() + MINUTE, DEFAULT_MINUTE);
        setSummary(String.format(FORMAT, hour, minute));
    }

    @Override
    protected View onCreateDialogView() {
        mTimePicker = new android.widget.TimePicker(getContext());
        mTimePicker.setIs24HourView(true);

        return mTimePicker;
    }

    @Override
    protected void onBindDialogView(@NonNull View view) {
        super.onBindDialogView(view);
        mTimePicker.setCurrentHour(getSharedPreferences().getInt(getKey() + HOUR, DEFAULT_HOUR));
        mTimePicker.setCurrentMinute(getSharedPreferences().getInt(getKey() + MINUTE, DEFAULT_MINUTE));
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            mTimePicker.clearFocus();
            SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(getContext()).edit();

            int hour = mTimePicker.getCurrentHour();
            int minute = mTimePicker.getCurrentMinute();

            ed.putInt(getKey() + HOUR, hour);
            ed.putInt(getKey() + MINUTE, minute);
            ed.apply();

            setSummary(String.format(FORMAT, hour, minute));
        }
    }

}
