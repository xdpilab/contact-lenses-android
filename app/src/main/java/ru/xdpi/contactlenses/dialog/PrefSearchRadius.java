package ru.xdpi.contactlenses.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.Utility;

public class PrefSearchRadius extends DialogPreference {
    public static final String TAG = "dialog." + PrefSearchQuery.class.getSimpleName();
    public static final String SEARCH_RADIUS = "sp_search_radius";
    private static final int LAYOUT = R.layout.fr_dialog_pref_search_radius;
    private final SharedPreferences mSp;
    private Editor mEd;
    private EditText mSearchRadius;

    @SuppressLint("CommitPrefEdits")
    public PrefSearchRadius(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(LAYOUT);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        mSp = PreferenceManager.getDefaultSharedPreferences(context);
        mEd = PreferenceManager.getDefaultSharedPreferences(context).edit();

        if ("".equals(mSp.getString(SEARCH_RADIUS, "")))
            setSummary(R.string.default_search_radius);
        else
            setSummary(mSp.getString(SEARCH_RADIUS, ""));
    }

    @Override
    protected void onBindDialogView(@NonNull View view) {
        mSearchRadius = view.findViewById(R.id.edit_search_query);

        if ("".equals(mSp.getString(SEARCH_RADIUS, "")))
            mSearchRadius.setText(R.string.default_search_radius);
        else
            mSearchRadius.setText(mSp.getString(SEARCH_RADIUS, ""));

        Utility.setCursorToEnd(mSearchRadius);
        super.onBindDialogView(view);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            mEd.putString(SEARCH_RADIUS, mSearchRadius.getText().toString());
            mEd.apply();

            setSummary(mSearchRadius.getText().toString());
        }
    }

}
