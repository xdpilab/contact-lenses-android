package ru.xdpi.contactlenses.dialog;

import android.app.DialogFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.model.Recipe;
import ru.xdpi.contactlenses.utility.LOG;
import ru.xdpi.contactlenses.utility.PanelColor;
import ru.xdpi.contactlenses.utility.ToastMessage;

public class RecipeEdit extends DialogFragment implements OnClickListener, OnSharedPreferenceChangeListener {
    public static final String TAG = "dialog." + RecipeEdit.class.getSimpleName();
    public static final String OPEN = "open_recipe_edit_dialog";
    private OnEditRecipeListener mOnEditRecipeListener;
    private EditText mRecipeName, mLensesName, mDaysBeforeChange, mSphLeft, mSphRight, mCylLeft,
            mCylRight, mAxLeft, mAxRight, mBcLeft, mBcRight, mDiaLeft, mDiaRight, mAddLeft,
            mAddRight, mExtraLeft, mExtraRight;
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private int mIndex;

    public RecipeEdit() {

    }

    public void setOnEditActionListener(OnEditRecipeListener listener) {
        mOnEditRecipeListener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);

        mIndex = getArguments().getInt(ru.xdpi.contactlenses.fragment.Recipe.INDEX_RECIPE);

        LOG.d(TAG, ru.xdpi.contactlenses.fragment.Recipe.INDEX_RECIPE + " " + mIndex);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fr_dialog_edit, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        // auto keyboard
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        mLayout = v.findViewById(R.id.recipe_edit_bg);
        mRecipeName = v.findViewById(R.id.EditText55);
        mLensesName = v.findViewById(R.id.EditText09);
        mDaysBeforeChange = v.findViewById(R.id.editText3);
        mSphLeft = v.findViewById(R.id.editText08);
        mSphRight = v.findViewById(R.id.EditText07);
        mCylLeft = v.findViewById(R.id.EditText04);
        mCylRight = v.findViewById(R.id.EditText03);
        mAxLeft = v.findViewById(R.id.EditText02);
        mAxRight = v.findViewById(R.id.EditText01);
        mBcLeft = v.findViewById(R.id.editText1);
        mBcRight = v.findViewById(R.id.editText2);
        mDiaLeft = v.findViewById(R.id.editText6);
        mDiaRight = v.findViewById(R.id.editText7);
        mAddLeft = v.findViewById(R.id.editText9);
        mAddRight = v.findViewById(R.id.editText10);
        mExtraLeft = v.findViewById(R.id.EditText10);
        mExtraRight = v.findViewById(R.id.EditText11);
        Button okBtn = v.findViewById(R.id.button1);
        Button cancelBtn = v.findViewById(R.id.button2);

        okBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

        try {
            load();
        } catch (IndexOutOfBoundsException ignored) {
            LOG.e(TAG, "IndexOutOfBoundsException " + ignored.toString());
        }

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                if (mRecipeName.length() == 0)
                    ToastMessage.toastL(getActivity(), R.string.toast_field_null, true);
                else {
                    try {
                        Recipe recipe = new Recipe();

                        recipe.setRecipeName(mRecipeName.getText().toString());
                        recipe.setLensesName(mLensesName.getText().toString());
                        recipe.setDaysBeforeChange(mDaysBeforeChange.getText().toString());

                        recipe.setSphLeft(mSphLeft.getText().toString());
                        recipe.setSphRight(mSphRight.getText().toString());

                        recipe.setCylLeft(mCylLeft.getText().toString());
                        recipe.setCylRight(mCylRight.getText().toString());

                        recipe.setAxLeft(mAxLeft.getText().toString());
                        recipe.setAxRight(mAxRight.getText().toString());

                        recipe.setBcLeft(mBcLeft.getText().toString());
                        recipe.setBcRight(mBcRight.getText().toString());

                        recipe.setDiaLeft(mDiaLeft.getText().toString());
                        recipe.setDiaRight(mDiaRight.getText().toString());

                        recipe.setAddLeft(mAddLeft.getText().toString());
                        recipe.setAddRight(mAddRight.getText().toString());

                        recipe.setExtraLeft(mExtraLeft.getText().toString());
                        recipe.setExtraRight(mExtraRight.getText().toString());

                        List<Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();

                        if (!recipes.isEmpty()) {
                            DatabaseFactory.getHelper().getRecipeDAO().deleteRowByRecipeName(mRecipeName.getText().toString());
                            DatabaseFactory.getHelper().getRecipeDAO().create(recipe);
                            LOG.d(TAG, "recipes - not empty");
                        } else {
                            DatabaseFactory.getHelper().getRecipeDAO().create(recipe);
                            LOG.d(TAG, "recipes - empty");
                        }

                        mOnEditRecipeListener.onGetEditRecipeText(
                                mRecipeName.getText().toString(), mLensesName.getText().toString(), mDaysBeforeChange.getText().toString(),
                                mSphLeft.getText().toString(), mSphRight.getText().toString(),
                                mCylLeft.getText().toString(), mCylRight.getText().toString(),
                                mAxLeft.getText().toString(), mAxRight.getText().toString(),
                                mBcLeft.getText().toString(), mBcRight.getText().toString(),
                                mDiaLeft.getText().toString(), mDiaRight.getText().toString(),
                                mAddLeft.getText().toString(), mAddRight.getText().toString(),
                                mExtraLeft.getText().toString(), mExtraRight.getText().toString());

                    } catch (NullPointerException | SQLException e) {
                        LOG.e(TAG, "NullPointerException | SQLException");
                    }

                    dismiss();
                }

                break;

            case R.id.button2:
                dismiss();
                break;

            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    void load() {
        try {
            List<Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
            if (!recipes.isEmpty()) {
                mRecipeName.setText(recipes.get(mIndex).getRecipeName());
                mLensesName.setText(recipes.get(mIndex).getLensesName());
                mDaysBeforeChange.setText(recipes.get(mIndex).getDaysBeforeChange());

                mSphLeft.setText(recipes.get(mIndex).getSphLeft());
                mSphRight.setText(recipes.get(mIndex).getSphRight());

                mCylLeft.setText(recipes.get(mIndex).getCylLeft());
                mCylRight.setText(recipes.get(mIndex).getCylRight());

                mAxLeft.setText(recipes.get(mIndex).getAxLeft());
                mAxRight.setText(recipes.get(mIndex).getAxRight());

                mBcLeft.setText(recipes.get(mIndex).getBcLeft());
                mBcRight.setText(recipes.get(mIndex).getBcRight());

                mDiaLeft.setText(recipes.get(mIndex).getDiaLeft());
                mDiaRight.setText(recipes.get(mIndex).getDiaRight());

                mAddLeft.setText(recipes.get(mIndex).getAddLeft());
                mAddRight.setText(recipes.get(mIndex).getAddRight());

                mExtraLeft.setText(recipes.get(mIndex).getExtraLeft());
                mExtraRight.setText(recipes.get(mIndex).getExtraRight());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        // cursor to end
        mRecipeName.setSelection(mRecipeName.getText().length());
    }

    private void setAppTheme(SharedPreferences sp) {
        PanelColor color = new PanelColor();
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        color.setBgFragment(sp, key, mLayout);
    }

}
