package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;

import ru.xdpi.contactlenses.R;

public class NoGps extends AlertDialog.Builder {
    public NoGps(final Context context) {
        super(context);
        setMessage(R.string.dialog_gps);
        setCancelable(false);
        setPositiveButton(R.string.dialog_rta_ok, (dialog, which) -> {
            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            context.startActivity(i);
        });
        setNeutralButton(R.string.cancel, (dialog, which) -> dialog.cancel());
    }

}