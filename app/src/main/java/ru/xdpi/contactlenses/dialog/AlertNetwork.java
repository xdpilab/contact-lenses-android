package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.Context;

import ru.xdpi.contactlenses.R;

public class AlertNetwork extends AlertDialog.Builder {

    public AlertNetwork(final Context context) {
        super(context);
        setMessage(R.string.dialog_network);
        setCancelable(false);
        setPositiveButton(R.string.ok, null);
    }

}
