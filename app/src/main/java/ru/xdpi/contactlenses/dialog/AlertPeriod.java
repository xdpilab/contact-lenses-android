package ru.xdpi.contactlenses.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import ru.xdpi.contactlenses.BuildType;
import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.ConstantFlavor;
import ru.xdpi.contactlenses.R;

public class AlertPeriod extends AlertDialog.Builder {

    public AlertPeriod(final Context context) {
        super(context);

        setMessage(R.string.dialog_message);
        if (ConstantFlavor.BUILD_TYPE == BuildType.FREE) {
            setPositiveButton(R.string.dialog_ok,
                    (dialog, which) -> {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.INSTANCE.getURL_MARKET_CONTACT_LENSES_PRO()));
                        context.startActivity(i);
                    });
        }
        setNegativeButton(R.string.dialog_cancel, null);
    }

}
