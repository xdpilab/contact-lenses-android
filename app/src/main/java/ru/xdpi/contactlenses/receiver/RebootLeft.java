package ru.xdpi.contactlenses.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.xdpi.contactlenses.fragment.Lenses;
import ru.xdpi.contactlenses.service.AlarmLeft;

public class RebootLeft extends BroadcastReceiver {
    private static final String L_TOGGLE = "l_left";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Lenses.LToggle = sp.getInt(L_TOGGLE, 0);
        if (Lenses.LToggle == 1) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                intent = new Intent(context, AlarmLeft.class);
                context.startService(intent);
            }
        }
    }

}
