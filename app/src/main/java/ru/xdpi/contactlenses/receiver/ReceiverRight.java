package ru.xdpi.contactlenses.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import ru.xdpi.contactlenses.service.ServiceNotification;

public class ReceiverRight extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        intent = new Intent(context, ServiceNotification.class);
        context.startService(intent);
    }

}
