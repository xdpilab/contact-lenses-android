package ru.xdpi.contactlenses.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.xdpi.contactlenses.fragment.Lenses;
import ru.xdpi.contactlenses.service.AlarmRight;

public class RebootRight extends BroadcastReceiver {
    private static final String R_TOGGLE = "r_right";

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        Lenses.RToggle = sp.getInt(R_TOGGLE, 0);
        if (Lenses.RToggle == 1) {
            if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
                intent = new Intent(context, AlarmRight.class);
                context.startService(intent);
            }
        }
    }

}
