package ru.xdpi.contactlenses.api;

public class GoogleServer {
    private static final String MAPS_API = "https://maps.googleapis.com/maps/api/";

    public static final String OK = "OK";
    public static final String ZERO_RESULTS = "ZERO_RESULTS";
    public static final String REQUEST_DENIED = "REQUEST_DENIED";

    public static class API {
        public static class Place {
            private static final String URL = MAPS_API + "place/";

            /**
             * https://developers.google.com/places/documentation/search?hl=ru
             * http://stackoverflow.com/questions/14654758/google-places-api-request-denied-for-android-autocomplete-even-with-the-right-a
             */
            public static class Method {
                public static final String PLACE_NEARBY_SEARCH = URL + "nearbysearch/json?";
            }

            public static class Param {
                // required
                public static final String KEY = "key";
                public static final String LOCATION = "location";
                public static final String RADIUS = "radius";
                public static final String SENSOR = "sensor";
                // extra
                public static final String KEYWORD = "keyword";
                public static final String LANGUAGE = "language";
                public static final String NAME = "name";
            }
        }
    }

}