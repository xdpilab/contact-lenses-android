package ru.xdpi.contactlenses.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.model.Info;

public class InfoDAO extends BaseDaoImpl<Info, Integer> {

    protected InfoDAO(ConnectionSource connectionSource, Class<Info> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Info> getAll() throws SQLException {
        return this.queryForAll();
    }

    public void deleteRowByInfoDescription(String description) throws SQLException {
        DeleteBuilder<Info, Integer> deleteBuilder = DatabaseFactory.getHelper().getUsefulInfoDAO().deleteBuilder();
        deleteBuilder.where().eq(Info.DESCRIPTION, description);
        deleteBuilder.delete();
    }

}
