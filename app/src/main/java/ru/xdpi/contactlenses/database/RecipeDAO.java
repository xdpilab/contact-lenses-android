package ru.xdpi.contactlenses.database;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.model.Recipe;

public class RecipeDAO extends BaseDaoImpl<Recipe, Integer> {

    protected RecipeDAO(ConnectionSource connectionSource, Class<Recipe> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Recipe> getAll() throws SQLException {
        return this.queryForAll();
    }

    public void deleteRowByRecipeName(String recipeName) throws SQLException {
        DeleteBuilder<Recipe, Integer> deleteBuilder = DatabaseFactory.getHelper().getRecipeDAO().deleteBuilder();
        deleteBuilder.where().eq(Recipe.RECIPE_NAME, recipeName);
        deleteBuilder.delete();
    }

    public void updateRowByRecipeName(String recipeName) throws SQLException {
        RecipeDAO recipeDAO = DatabaseFactory.getHelper().getRecipeDAO();
        UpdateBuilder<Recipe, Integer> updateBuilder = recipeDAO.updateBuilder();
        updateBuilder.where().eq(Recipe.LENSES_NAME, recipeName);
        updateBuilder.updateColumnValue(Recipe.DAYS_BEFORE_CHANGE, "100");
        updateBuilder.update();
    }

}
