package ru.xdpi.contactlenses.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.xdpi.contactlenses.model.Recipe;
import ru.xdpi.contactlenses.model.Info;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = "database."+DatabaseHelper.class.getSimpleName();
    private static final String DATABASE_NAME = "contactlenses.db";
    private static final int DATABASE_VERSION = 6;
    private RecipeDAO mRecipeDao = null;
    private InfoDAO mInfoDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Recipe.class);
            TableUtils.createTable(connectionSource, Info.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Recipe.class, true);
            TableUtils.dropTable(connectionSource, Info.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    public RecipeDAO getRecipeDAO() throws SQLException {
        if (mRecipeDao == null) {
            mRecipeDao = new RecipeDAO(getConnectionSource(), Recipe.class);
        }
        return mRecipeDao;
    }

    public InfoDAO getUsefulInfoDAO() throws SQLException {
        if (mInfoDao == null) {
            mInfoDao = new InfoDAO(getConnectionSource(), Info.class);
        }
        return mInfoDao;
    }

    @Override
    public void close(){
        super.close();
        mRecipeDao = null;
        mInfoDao = null;
    }

}