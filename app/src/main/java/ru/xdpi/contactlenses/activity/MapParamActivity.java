package ru.xdpi.contactlenses.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.fragment.MapParam;

public class MapParamActivity extends AppCompatActivity {
    private static final int LAYOUT = R.layout.activity_map_param;
    private static final int TOOLBAR = R.id.toolbar;
    private static final int CONTENT = R.id.activity_map_param_content_frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolbar();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(CONTENT, new MapParam());
        transaction.commit();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setTitle(R.string.menu_search_params);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

}