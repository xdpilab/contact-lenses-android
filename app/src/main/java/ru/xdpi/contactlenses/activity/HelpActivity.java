package ru.xdpi.contactlenses.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.UUID;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.model.Help;
import ru.xdpi.contactlenses.model.HelpLab;

public class HelpActivity extends AppCompatActivity {
    private static final int LAYOUT = R.layout.activity_help;
    private static final int TOOLBAR = R.id.toolbar;

    private ArrayList<Help> mHelps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolbar();

        mHelps = HelpLab.get(this).getHelps();
        FragmentManager fm = getFragmentManager();
        ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public CharSequence getPageTitle(int position) {
                return mHelps.get(position).getTitle();
            }

            @Override
            public int getCount() {
                return mHelps.size();
            }

            @Override
            public Fragment getItem(int pos) {
                Help help = mHelps.get(pos);
                return ru.xdpi.contactlenses.fragment.Help.newInstance(help.getId());
            }
        });

        UUID helpId = (UUID) getIntent().getSerializableExtra(ru.xdpi.contactlenses.fragment.Help.TAG);
        for (int i = 0; i < mHelps.size(); i++) {
            if (mHelps.get(i).getId().equals(helpId)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

}