package ru.xdpi.contactlenses.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.ConstantFlavor;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.dialog.DialogController;

public class AboutActivity extends AppCompatActivity {
    private static final int LAYOUT = R.layout.activity_about;
    private static final int TOOLBAR = R.id.toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolbar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                Intent help = new Intent(this, HelpActivity.class);
                startActivity(help);
                return true;

            case R.id.share:
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType(Constant.INSTANCE.getTEXT_PLAIN());
                share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                share.putExtra(Intent.EXTRA_TEXT, ConstantFlavor.CONTACT_LENSES_SHARE);
                startActivity(Intent.createChooser(share, getString(R.string.menu_share)));
                return true;

            case R.id.rate:
                DialogController.showRateApp(this);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

}