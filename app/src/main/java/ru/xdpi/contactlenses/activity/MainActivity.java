package ru.xdpi.contactlenses.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.xdpi.contactlenses.BuildConfig;
import ru.xdpi.contactlenses.BuildType;
import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.ConstantFlavor;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.dialog.DialogController;
import ru.xdpi.contactlenses.dialog.RateApp;
import ru.xdpi.contactlenses.fragment.Lenses;
import ru.xdpi.contactlenses.fragment.Recipe;
import ru.xdpi.contactlenses.fragment.UsefulInfo;
import ru.xdpi.contactlenses.googleservice.remote_config.ConstantFirebase;
import ru.xdpi.contactlenses.utility.Device;
import ru.xdpi.contactlenses.utility.ToastMessage;
import ru.xdpi.contactlenses.utility.Utility;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "activity." + MainActivity.class.getSimpleName();
    public static int INDEX_RECIPE_PUBLIC;

    private static final int LAYOUT = R.layout.activity_main;
    private static final int TOOLBAR = R.id.toolbar;
    private static final int DRAWER = R.id.activity_main_drawer_layout;
    private static final int NAVIGATION_VIEW = R.id.navigation;

    private static final int NAV_MENU_UPDATE = R.id.menu_update;
    private static final int NAV_MENU_SETTINGS = R.id.settings;
    private static final int NAV_MENU_WHERE_BUY = R.id.where_buy;
    private static final int NAV_MENU_SEND_RECIPE = R.id.send_recipe;
    private static final int NAV_MENU_NO_ADS = R.id.no_advertising;
    private static final int NAV_MENU_OUR_APPS = R.id.our_apps;
    private static final int NAV_MENU_ABOUT = R.id.about;

    private static final int GROUP_NAV_MENU_OUR_APPS = R.id.group_menu_our_apps;
    private static final int NAV_STOPWATCH = R.id.menu_stopwatch;
    private static final int NAV_CALCULATOR = R.id.menu_calculator;
    private static final int NAV_CURIOUS_BUNNY = R.id.menu_curious_bunny;

    private static final int NUM_PAGES = 3;
    private static final int OFF_PAGE_LIMIT = 2;

    private final Handler mDrawerActionHandler = new Handler();
    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    long mCacheExpiration = 3600; // 1 hour in seconds.
    private FirebaseRemoteConfig mFirebaseRemoteConfig;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private NavigationView mNavigationView;
    private MenuItem mMenuItemUpdateApp;

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                ToastMessage.toastL(MainActivity.this, getString(R.string.toast_device_not_supported));
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPlayServices();

        setTitle(R.string.title_lenses);
        setContentView(LAYOUT);

        initViewPager();
        initNavMenu();

        Menu navMenu = mNavigationView.getMenu();

        // set color of update menu item
        mMenuItemUpdateApp = navMenu.findItem(NAV_MENU_UPDATE);
        SpannableString updateApp = new SpannableString(getString(R.string.navigation_menu_update_app).toUpperCase());
        updateApp.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.update_app)), 0, updateApp.length(), 0);
        mMenuItemUpdateApp.setTitle(updateApp);

        // check new version app
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.fetch(mCacheExpiration)
                .addOnSuccessListener(aVoid -> {
                    // once the config is successfully fetched it must be activated before newly fetched values are returned.
                    mFirebaseRemoteConfig.activateFetched();

                    if (mFirebaseRemoteConfig.getLong(ConstantFirebase.CURRENT_VERSION_CODE) > Device.getAppVersion(MainActivity.this))
                        mMenuItemUpdateApp.setVisible(true);
                    else
                        mMenuItemUpdateApp.setVisible(false);
                })
                .addOnFailureListener(e -> {

                });
    }

    @Override
    protected void onStart() {
        super.onStart();
        RateApp.onStart(this);
        RateApp.showRateDialogIfNeeded(this);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawer(GravityCompat.START);
        else if (getFragmentManager().getBackStackEntryCount() == 0)
            finish();
        else
            getFragmentManager().popBackStack();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void initViewPager() {
        ViewPager viewPager = findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(OFF_PAGE_LIMIT);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        setTitle(R.string.title_lenses);
                        break;

                    case 1:
                        setTitle(R.string.title_recipe);
                        break;

                    case 2:
                        setTitle(R.string.title_useful_info);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initNavMenu() {
        Toolbar toolbar = findViewById(TOOLBAR);
        setSupportActionBar(toolbar);

        mDrawerLayout = findViewById(DRAWER);
        mNavigationView = findViewById(NAVIGATION_VIEW);
        Menu menuNav = mNavigationView.getMenu();
        MenuItem noAds = menuNav.findItem(NAV_MENU_NO_ADS);
        MenuItem ourApps = menuNav.findItem(NAV_MENU_OUR_APPS);

        // need for color icons in nav menu
        mNavigationView.setItemIconTintList(null);

        // set different menu items for free and paid version
        if (ConstantFlavor.BUILD_TYPE == BuildType.PAID)
            menuNav.setGroupVisible(GROUP_NAV_MENU_OUR_APPS, false);
        else {
            noAds.setVisible(true);
            ourApps.setVisible(false);
        }

        // navigation open/close listener
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        // navigation click listener
        mNavigationView.setNavigationItemSelectedListener(item -> {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            mDrawerActionHandler.postDelayed(() -> navigate(item.getItemId()), DRAWER_CLOSE_DELAY_MS);
            return true;
        });
    }

    private void openGooglePlay() {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(ConstantFlavor.CONTACT_LENSES));
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void navigate(final int itemId) {
        switch (itemId) {
            // Update up ===========================================================================
            case NAV_MENU_UPDATE:
                openGooglePlay();
                break;

            // Settings ============================================================================
            case NAV_MENU_SETTINGS:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;

            // Where buy ===========================================================================
            case NAV_MENU_WHERE_BUY:
                if (!Device.isOnline(this))
                    DialogController.showNoConnection(this);

                else if (!Device.isGpsEnabled(this))
                    DialogController.showNoGps(this);

                else {
                    Intent whereBuy = new Intent(MainActivity.this, MapActivity.class);
                    startActivity(whereBuy);
                }
                break;

            // Send recipe =========================================================================
            case NAV_MENU_SEND_RECIPE:
                Intent sendRecipe = new Intent(Intent.ACTION_SEND);
                sendRecipe.setType(Constant.INSTANCE.getTEXT_PLAIN());
                sendRecipe.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_search_query));
                List<ru.xdpi.contactlenses.model.Recipe> recipes = new ArrayList<>();
                try {
                    recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
                    if (!recipes.isEmpty()) {
                        sendRecipe.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.title_recipe) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getRecipeName() +
                                "\n" + getResources().getString(R.string.lenses) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getLensesName() +
                                "\n" + getResources().getString(R.string.days) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getDaysBeforeChange() +
                                "\n" +
                                "\n" + getResources().getString(R.string.sph) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getSphLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getSphRight() +
                                "\n" + getResources().getString(R.string.cyl) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getCylLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getCylRight() +
                                "\n" + getResources().getString(R.string.ax) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getAxLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getAxRight() +
                                "\n" + getResources().getString(R.string.bc) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getBcLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getBcRight() +
                                "\n" + getResources().getString(R.string.dia) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getDiaLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getDiaRight() +
                                "\n" + getResources().getString(R.string.add) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getAddLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getAddRight() +
                                "\n" + getResources().getString(R.string.extra) + ":  " + recipes.get(INDEX_RECIPE_PUBLIC).getExtraLeft() + " -- " + recipes.get(INDEX_RECIPE_PUBLIC).getExtraRight() +
                                "\n" +
                                "");
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                // check recipe list is exist
                if (!recipes.isEmpty())
                    startActivity(Intent.createChooser(sendRecipe, getString(R.string.menu_send_recipe)));
                else
                    DialogController.showAlert(this, getString(R.string.dialog_no_recipe));
                break;

            // Remove Ads ==========================================================================
            case NAV_MENU_NO_ADS:
                try {
                    Intent noAds = new Intent(Intent.ACTION_VIEW);
                    noAds.setData(Uri.parse(Constant.INSTANCE.getURL_MARKET_CONTACT_LENSES_PRO()));
                    startActivity(noAds);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            // Our Apps ============================================================================
            case NAV_MENU_OUR_APPS:
                try {
                    Intent ourApps = new Intent(Intent.ACTION_VIEW);
                    ourApps.setData(Uri.parse(Constant.INSTANCE.getURL_MARKET_XDPI_LAB()));
                    startActivity(ourApps);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;

            // About ===============================================================================
            case NAV_MENU_ABOUT:
                Intent about = new Intent(this, AboutActivity.class);
                startActivity(about);
                break;

            // Stopwatch ===========================================================================
            case NAV_STOPWATCH:
                if (Utility.isPackageInstalled(Constant.INSTANCE.getPACKAGE_STOPWATCH(), this)) {
                    Intent launchContactLenses = getPackageManager().getLaunchIntentForPackage(Constant.INSTANCE.getPACKAGE_STOPWATCH());
                    startActivity(launchContactLenses);
                } else {
                    try {
                        Intent contactLenses = new Intent(Intent.ACTION_VIEW);
                        contactLenses.setData(Uri.parse(Constant.INSTANCE.getURL_MARKET_STOPWATCH()));
                        startActivity(contactLenses);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;

            // Calculator===========================================================================
            case NAV_CALCULATOR:
                if (Utility.isPackageInstalled(Constant.INSTANCE.getPACKAGE_CALCULATOR(), this)) {
                    Intent launchContactLenses = getPackageManager().getLaunchIntentForPackage(Constant.INSTANCE.getPACKAGE_CALCULATOR());
                    startActivity(launchContactLenses);
                } else {
                    try {
                        Intent contactLenses = new Intent(Intent.ACTION_VIEW);
                        contactLenses.setData(Uri.parse(Constant.INSTANCE.getURL_MARKET_CALCULATOR()));
                        startActivity(contactLenses);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;

            // Curious Bunny =======================================================================
            case NAV_CURIOUS_BUNNY:
                try {
                    Intent curiosBunnyFree = new Intent(Intent.ACTION_VIEW);
                    curiosBunnyFree.setData(Uri.parse(Constant.INSTANCE.getURL_MARKET_CURIOUS_BUNNY_FREE()));
                    startActivity(curiosBunnyFree);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Lenses();

                case 1:
                    return new Recipe();

                case 2:
                    return new UsefulInfo();

                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

}