package ru.xdpi.contactlenses.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.utility.ToastMessage;

/**
 * http://developer.android.com/reference/android/webkit/WebView.html
 */
@SuppressLint("SetJavaScriptEnabled")
public class WebViewActivity extends AppCompatActivity {
    public static final String TAG = "activity." + WebViewActivity.class.getSimpleName();

    private static final int LAYOUT = R.layout.activity_web_view;
    private static final int TOOLBAR = R.id.toolbar;

    public static final String TARGET_URL = "target_url";
    public static final String TITLE = "title";

    private WebView mWebView;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);

        String title = getIntent().getExtras().getString(TITLE);
        if (title != null)
            initToolbar(title);
        else
            initToolbar("");

        mWebView = findViewById(R.id.web_view);
        mProgressBar = findViewById(R.id.progressBar);

        WebSettings settings = mWebView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);

        final AppCompatActivity activity = this;
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                activity.setProgress(progress * 1000);
            }
        });

        mWebView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                mProgressBar.setVisibility(View.GONE);
                ToastMessage.toastL(activity, description);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }
        });

        mWebView.loadUrl(getIntent().getExtras().getString(TARGET_URL));
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    private void initToolbar(String title) {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

}