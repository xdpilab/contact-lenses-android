package ru.xdpi.contactlenses.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.fragment.Settings;

public class SettingsActivity extends AppCompatActivity {
    private static final int LAYOUT = R.layout.activity_settings;
    private static final int TOOLBAR = R.id.toolbar;
    private static final int CONTENT = R.id.activity_settings_content_frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolbar();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(CONTENT, new Settings());
        transaction.commit();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setTitle(R.string.menu_settings);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

}