package ru.xdpi.contactlenses.activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import ru.xdpi.contactlenses.AppController;
import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.api.GoogleServer;
import ru.xdpi.contactlenses.api.GsonRequest;
import ru.xdpi.contactlenses.dialog.PrefSearchQuery;
import ru.xdpi.contactlenses.dialog.PrefSearchRadius;
import ru.xdpi.contactlenses.model.maps.PlacesNearby;
import ru.xdpi.contactlenses.model.maps.Result;
import ru.xdpi.contactlenses.utility.Device;
import ru.xdpi.contactlenses.utility.ToastMessage;

/**
 * https://developers.google.com/maps/documentation/android-api/location
 */
public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    public static final String TAG = "activity." + MapActivity.class.getSimpleName();

    private static final int LAYOUT = R.layout.activity_map;
    private static final int TOOLBAR = R.id.toolbar;

    private static final String URL_PLACE_NEARBY_SEARCH = GoogleServer.API.Place.Method.PLACE_NEARBY_SEARCH;

    private static final int MENU_SEARCH_PARAMS = R.id.menu_search_params;

    private SharedPreferences mSp;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private static final int MAP_ZOOM = 12;

    private final static int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 777;
    private ProgressBar mProgressBar;
    private View mCoordinatorLayoutView;
    private LocationRequest mLocationRequest;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // if request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    getMapLocation();

                else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Snackbar.make(mCoordinatorLayoutView, R.string.access_denied, Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.more).toUpperCase(), view -> {
                                try {
                                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.INSTANCE.getMAPS_PERMISSION_HELP()));
                                    startActivity(myIntent);
                                } catch (IndexOutOfBoundsException e) {
                                    e.printStackTrace();
                                }
                            })
                            .show();
                } else
                    finish();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                // show an explanation to the user *asynchronously* -- don't block this thread waiting for the user's response! After the user sees the explanation, try again to request the permission.

            } else {
                // no explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
            return;
        }

        getMapLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT);
        initToolbar();

        // create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)    // 10 seconds
                .setFastestInterval(1000); // 1 second

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mSp = PreferenceManager.getDefaultSharedPreferences(this);

        mProgressBar = findViewById(R.id.preloader);
        mCoordinatorLayoutView = findViewById(R.id.snackbarPosition);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        requestGetNearShops(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        AppController.getInstance().cancelPendingRequests(TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_SEARCH_PARAMS:
                Intent requestParams = new Intent(this, MapParamActivity.class);
                startActivity(requestParams);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(TOOLBAR);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    private void requestGetNearShops(double latitude, double longitude) {
        mProgressBar.setVisibility(View.VISIBLE);
        String radius = mSp.getString(PrefSearchRadius.SEARCH_RADIUS, "");
        if (radius.equals(""))
            radius = getString(R.string.default_search_radius);

        String keyWord = mSp.getString(PrefSearchQuery.SEARCH_QUERY, "");
        if (keyWord.equals(""))
            keyWord = getString(R.string.default_search_query);

        final String url = URL_PLACE_NEARBY_SEARCH +
                GoogleServer.API.Place.Param.LOCATION + "=" + latitude + "," + longitude + "&" +
                GoogleServer.API.Place.Param.RADIUS + "=" + radius + "&" +
                GoogleServer.API.Place.Param.SENSOR + "=" + String.valueOf(Device.hasGpsSensor(this)) + "&" +
                GoogleServer.API.Place.Param.KEYWORD + "=" + (Uri.encode(keyWord)) + "&" +
                GoogleServer.API.Place.Param.KEY + "=" + getString(R.string.server_key) + "";
        doRequest(url);
    }

    private void doRequest(String url) {
        GsonRequest<PlacesNearby> jsObjRequest = new GsonRequest<PlacesNearby>(Request.Method.GET, url, PlacesNearby.class,
                response -> {
                    switch (response.getStatus()) {
                        case GoogleServer.OK:
                            List<Result> results = response.getResults();
                            for (Result result : results) {
                                LatLng placePoint = new LatLng(result.getGeometry().getLocation().getLat(), result.getGeometry().getLocation().getLng());
                                mMap.addMarker(new MarkerOptions().position(placePoint).title(result.getName()));
                            }
                            LatLng locationPoint = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            mMap.addMarker(new MarkerOptions().position(locationPoint));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(locationPoint, MAP_ZOOM));

                            if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                                return;
                            mMap.setMyLocationEnabled(true);

                            break;

                        case GoogleServer.ZERO_RESULTS:
                            ToastMessage.toastL(MapActivity.this, R.string.zero_results, true);
                            break;

                        case GoogleServer.REQUEST_DENIED:

                            break;

                        default:
                            if (response.getErrorMessage() != null)
                                ToastMessage.toastL(MapActivity.this, response.getErrorMessage());
                            break;
                    }

                    mProgressBar.setVisibility(View.GONE);
                },
                error -> {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null)
                        ToastMessage.toastL(MapActivity.this, networkResponse.statusCode, false);

                    mProgressBar.setVisibility(View.GONE);
                }) {
        };

        jsObjRequest.setTag(TAG);
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    @Override
    public void onLocationChanged(Location location) {
        requestGetNearShops(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    }

    private void getMapLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        } else
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

}