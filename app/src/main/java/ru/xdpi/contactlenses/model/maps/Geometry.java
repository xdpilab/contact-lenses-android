package ru.xdpi.contactlenses.model.maps;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Geometry implements Parcelable {
    public static final String TAG = "model.maps" + Geometry.class.getSimpleName();

    public static final String LOCATION = "location";

    @SerializedName(LOCATION)
    private Location mLocation;

    public Geometry() {
       
    }

    public Location getLocation() {
        return mLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mLocation, flags);
    }

    public static final Parcelable.Creator<Geometry> CREATOR = new Parcelable.Creator<Geometry>() {
        public Geometry createFromParcel(Parcel in) {
            return new Geometry(in);
        }

        public Geometry[] newArray(int size) {
            return new Geometry[size];
        }
    };

    private Geometry(Parcel parcel) {
        parcel.readParcelable(Location.class.getClassLoader());
    }

}
