package ru.xdpi.contactlenses.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Useful info")
public class Info {
    public final static String ID = "id";
    public final static String LINK = "Link";
    public final static String DESCRIPTION = "Description";

    @DatabaseField(generatedId = true, columnName = ID)
    private int mId;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = LINK)
    private String mLink;

    @DatabaseField(canBeNull = true, dataType = DataType.STRING, columnName = DESCRIPTION)
    private String mDescription;

    public Info() {

    }

    public Info(String link, String description) {
        mLink = link;
        mDescription = description;
    }

    public String getLink() {
        return mLink;
    }

    public String getDescription() {
        return mDescription;
    }

}
