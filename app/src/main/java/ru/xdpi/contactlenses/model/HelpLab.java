package ru.xdpi.contactlenses.model;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

import ru.xdpi.contactlenses.R;

public class HelpLab {
    private ArrayList<Help> mHelps;
    private static HelpLab sHelpLab;

    private HelpLab(Context context) {
        mHelps = new ArrayList<>();

        final String[] titleList = context.getResources().getStringArray(
                R.array.help_list_title);

        final String[] descriptionList = context.getResources().getStringArray(
                R.array.help_list_description);

        for (int i = 0; i < titleList.length; i++) {
            Help help = new Help();
            help.setList(titleList[i], descriptionList[i]);
            mHelps.add(help);
        }
    }

    public static HelpLab get(Context context) {
        if (sHelpLab == null) {
            sHelpLab = new HelpLab(context.getApplicationContext());
        }
        return sHelpLab;
    }

    public ArrayList<Help> getHelps() {
        return mHelps;
    }

    public Help getInstruction(UUID id) {
        for (Help c : mHelps) {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }

}
