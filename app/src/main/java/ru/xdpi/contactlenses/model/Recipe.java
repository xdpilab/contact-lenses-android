package ru.xdpi.contactlenses.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "Recipe list")
public class Recipe {
    public final static String ID = "id";
    public final static String RECIPE_NAME = "Recipe name";
    public final static String LENSES_NAME = "Lenses name";
    public final static String DAYS_BEFORE_CHANGE = "Days before change";
    public final static String SPH_LEFT = "SPH left";
    public final static String SPH_RIGHT = "SPH right";
    public final static String CYL_LEFT = "CYL left";
    public final static String CYL_RIGHT = "CYL right";
    public final static String AX_LEFT = "AX left";
    public final static String AX_RIGHT = "AX right";
    public final static String BC_LEFT = "BC left";
    public final static String BC_RIGHT = "BC right";
    public final static String DIA_LEFT = "DIA left";
    public final static String DIA_RIGHT = "DIA right";
    public final static String ADD_LEFT = "ADD left";
    public final static String ADD_RIGHT = "ADD right";
    public final static String EXTRA_LEFT = "Extra left";
    public final static String EXTRA_RIGHT = "Extra right";

    @DatabaseField(generatedId = true, columnName = ID)
    private int mId;

    @DatabaseField(canBeNull = false, unique = true, dataType = DataType.STRING, columnName = RECIPE_NAME)
    private String mRecipeName;

    @DatabaseField(columnName = LENSES_NAME)
    private String mLensesName;

    @DatabaseField(columnName = DAYS_BEFORE_CHANGE)
    private String mDaysBeforeChange;

    @DatabaseField(columnName = SPH_LEFT)
    private String mSphLeft;

    @DatabaseField(columnName = SPH_RIGHT)
    private String mSphRight;

    @DatabaseField(columnName = CYL_LEFT)
    private String mCylLeft;

    @DatabaseField(columnName = CYL_RIGHT)
    private String mCylRight;

    @DatabaseField(columnName = AX_LEFT)
    private String mAxLeft;

    @DatabaseField(columnName = AX_RIGHT)
    private String mAxRight;

    @DatabaseField(columnName = BC_LEFT)
    private String mBcLeft;

    @DatabaseField(columnName = BC_RIGHT)
    private String mBcRight;

    @DatabaseField(columnName = DIA_LEFT)
    private String mDiaLeft;

    @DatabaseField(columnName = DIA_RIGHT)
    private String mDiaRight;

    @DatabaseField(columnName = ADD_LEFT)
    private String mAddLeft;

    @DatabaseField(columnName = ADD_RIGHT)
    private String mAddRight;

    @DatabaseField(columnName = EXTRA_LEFT)
    private String mExtraLeft;

    @DatabaseField(columnName = EXTRA_RIGHT)
    private String mExtraRight;

    public Recipe() {
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getRecipeName() {
        return mRecipeName;
    }

    public void setRecipeName(String recipeName) {
        mRecipeName = recipeName;
    }

    public String getLensesName() {
        return mLensesName;
    }

    public void setLensesName(String lensesName) {
        mLensesName = lensesName;
    }

    public String getDaysBeforeChange() {
        return mDaysBeforeChange;
    }

    public void setDaysBeforeChange(String daysBeforeChange) {
        mDaysBeforeChange = daysBeforeChange;
    }

    public String getSphLeft() {
        return mSphLeft;
    }

    public void setSphLeft(String sphLeft) {
        mSphLeft = sphLeft;
    }

    public String getSphRight() {
        return mSphRight;
    }

    public void setSphRight(String sphRight) {
        mSphRight = sphRight;
    }

    public String getCylLeft() {
        return mCylLeft;
    }

    public void setCylLeft(String cylLeft) {
        mCylLeft = cylLeft;
    }

    public String getCylRight() {
        return mCylRight;
    }

    public void setCylRight(String cylRight) {
        mCylRight = cylRight;
    }

    public String getAxLeft() {
        return mAxLeft;
    }

    public void setAxLeft(String axLeft) {
        mAxLeft = axLeft;
    }

    public String getAxRight() {
        return mAxRight;
    }

    public void setAxRight(String axRight) {
        mAxRight = axRight;
    }

    public String getBcLeft() {
        return mBcLeft;
    }

    public void setBcLeft(String bcLeft) {
        mBcLeft = bcLeft;
    }

    public String getBcRight() {
        return mBcRight;
    }

    public void setBcRight(String bcRight) {
        mBcRight = bcRight;
    }

    public String getDiaLeft() {
        return mDiaLeft;
    }

    public void setDiaLeft(String diaLeft) {
        mDiaLeft = diaLeft;
    }

    public String getDiaRight() {
        return mDiaRight;
    }

    public void setDiaRight(String diaRight) {
        mDiaRight = diaRight;
    }

    public String getAddLeft() {
        return mAddLeft;
    }

    public void setAddLeft(String addLeft) {
        mAddLeft = addLeft;
    }

    public String getAddRight() {
        return mAddRight;
    }

    public void setAddRight(String addRight) {
        mAddRight = addRight;
    }

    public String getExtraLeft() {
        return mExtraLeft;
    }

    public void setExtraLeft(String extraLeft) {
        mExtraLeft = extraLeft;
    }

    public String getExtraRight() {
        return mExtraRight;
    }

    public void setExtraRight(String extraRight) {
        mExtraRight = extraRight;
    }

}
