package ru.xdpi.contactlenses.model.maps;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Result implements Parcelable {
    public static final String TAG = "model.maps" + Result.class.getSimpleName();

    public static final String FORMATTED_ADDRESS = "formatted_address";
    public static final String GEOMETRY = "geometry";
    public static final String ICON = "icon";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String RATING = "rating";
    public static final String REFERENCE = "reference";
    public static final String TYPES = "types";

    @SerializedName(FORMATTED_ADDRESS)
    private String mFormattedAddress;

    @SerializedName(GEOMETRY)
    private Geometry mGeometry;

    @SerializedName(ICON)
    private String mIcon;

    @SerializedName(ID)
    private String mId;

    @SerializedName(NAME)
    private String mName;

    @SerializedName(RATING)
    private double mRating;

    @SerializedName(REFERENCE)
    private String mReference;

    @SerializedName(TYPES)
    private List<String> mTypes = new ArrayList<>();

    public Result() {

    }

    public String getFormattedAddress() {
        return mFormattedAddress;
    }

    public Geometry getGeometry() {
        return mGeometry;
    }

    public String getIcon() {
        return mIcon;
    }

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public double getRating() {
        return mRating;
    }

    public String getReference() {
        return mReference;
    }

    public List<String> getTypes() {
        return mTypes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFormattedAddress);
        dest.writeParcelable(mGeometry, flags);
        dest.writeString(mIcon);
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeDouble(mRating);
        dest.writeString(mReference);
        dest.writeList(mTypes);
    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    private Result(Parcel parcel) {
        mFormattedAddress = parcel.readString();
        parcel.readParcelable(Geometry.class.getClassLoader());
        mIcon = parcel.readString();
        mId = parcel.readString();
        mName = parcel.readString();
        mRating = parcel.readDouble();
        mReference = parcel.readString();
        parcel.readList(mTypes, String.class.getClassLoader());
    }

}
