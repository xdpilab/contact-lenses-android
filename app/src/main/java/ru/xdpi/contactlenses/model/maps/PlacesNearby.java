package ru.xdpi.contactlenses.model.maps;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PlacesNearby implements Parcelable {
    public static final String TAG = "model.maps" + PlacesNearby.class.getSimpleName();

    public static final String ERROR_MESSAGE = "error_message";
    public static final String NEXT_PAGE_TOKEN = "next_page_token";
    public static final String HTML_ATTRIBUTIONS = "html_attributions";
    public static final String RESULTS = "results";
    public static final String STATUS = "status";

    @SerializedName(ERROR_MESSAGE)
    private String mErrorMessage;

    @SerializedName(NEXT_PAGE_TOKEN)
    private String mNextPageToken;

    @SerializedName(HTML_ATTRIBUTIONS)
    private List<String> mHtmlAttributions;

    @SerializedName(RESULTS)
    private List<Result> mResults = new ArrayList<>();

    @SerializedName(STATUS)
    private String mStatus;

    public PlacesNearby(String errorMessage, String nextPageToken, List<String> htmlAttributions,
                        List<Result> results, String status) {
        mErrorMessage = errorMessage;
        mNextPageToken = nextPageToken;
        mHtmlAttributions = htmlAttributions;
        mResults = results;
        mStatus = status;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public List<String> getHtmlAttributions() {
        return mHtmlAttributions;
    }

    public String getNextPageToken() {
        return mNextPageToken;
    }

    public List<Result> getResults() {
        return mResults;
    }

    public String getStatus() {
        return mStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mErrorMessage);
        dest.writeString(mNextPageToken);
        dest.writeList(mHtmlAttributions);
        dest.writeTypedList(mResults);
        dest.writeString(mStatus);
    }

    public static final Creator<PlacesNearby> CREATOR = new Creator<PlacesNearby>() {
        public PlacesNearby createFromParcel(Parcel in) {
            return new PlacesNearby(in);
        }

        public PlacesNearby[] newArray(int size) {
            return new PlacesNearby[size];
        }
    };

    private PlacesNearby(Parcel parcel) {
        mErrorMessage = parcel.readString();
        mNextPageToken = parcel.readString();
        parcel.readList(mHtmlAttributions, List.class.getClassLoader());
        parcel.readTypedList(mResults, Result.CREATOR);
        mStatus = parcel.readString();
    }

}
