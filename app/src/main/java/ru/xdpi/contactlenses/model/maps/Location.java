package ru.xdpi.contactlenses.model.maps;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Location implements Parcelable {
    public static final String TAG = "model.maps" + Location.class.getSimpleName();

    public static final String LAT = "lat";
    public static final String LNG = "lng";

    @SerializedName(LAT)
    private double mLat;

    @SerializedName(LNG)
    private double mLng;

    public Location() {

    }

    public double getLat() {
        return mLat;
    }

    public double getLng() {
        return mLng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(mLat);
        dest.writeDouble(mLng);
    }

    public static final Parcelable.Creator<Location> CREATOR = new Parcelable.Creator<Location>() {
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    private Location(Parcel parcel) {
        mLat = parcel.readDouble();
        mLng = parcel.readDouble();
    }

}
