package ru.xdpi.contactlenses.model;

import java.util.UUID;

public class Help {
    private UUID mId;
    private String mTitle;
    private String mDescription;

    public Help() {
        mId = UUID.randomUUID();
    }

    public UUID getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setList(String title, String description) {
        mTitle = title;
        mDescription = description;
    }

    @Override
    public String toString() {
        return mTitle;
    }

}
