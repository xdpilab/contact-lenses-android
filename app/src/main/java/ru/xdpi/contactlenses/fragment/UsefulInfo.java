package ru.xdpi.contactlenses.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.activity.WebViewActivity;
import ru.xdpi.contactlenses.adapter.UsefulInfoAdapter;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.dialog.AddInfo;
import ru.xdpi.contactlenses.dialog.NoConnection;
import ru.xdpi.contactlenses.dialog.OnAddInfoListener;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;
import ru.xdpi.contactlenses.model.Info;
import ru.xdpi.contactlenses.utility.Device;
import ru.xdpi.contactlenses.utility.PanelColor;

public class UsefulInfo extends Fragment implements OnSharedPreferenceChangeListener, View.OnClickListener, OnAddInfoListener {
    public static final String TAG = "fragment." + UsefulInfo.class.getSimpleName();
    private static final int LAYOUT = R.layout.fr_useful_info;

    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private List<Info> mInfoItems;
    private UsefulInfoAdapter mAdapter;

    public UsefulInfo() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            Analytics.setAppTracker(getActivity(), TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT, container, false);

        mLayout = v.findViewById(R.id.LinearLayout04);
        ListView usefulInfo = v.findViewById(R.id.useful_info);
        final Button addInfo = v.findViewById(R.id.add_info);

        addInfo.setOnClickListener(this);

        try {
            mInfoItems = DatabaseFactory.getHelper().getUsefulInfoDAO().getAll();
            mAdapter = new UsefulInfoAdapter(getActivity(), mInfoItems);
            usefulInfo.setAdapter(mAdapter);
            usefulInfo.setOnItemClickListener((parent, view, position, id) -> {
                if (!mInfoItems.isEmpty()) {
                    if (Device.isOnline(getActivity())) {
                        Intent i = new Intent(getActivity(), WebViewActivity.class);
                        i.putExtra(WebViewActivity.TARGET_URL, mInfoItems.get(position).getLink());
                        i.putExtra(WebViewActivity.TITLE, mInfoItems.get(position).getDescription());
                        startActivity(i);
                    } else
                        new NoConnection(getActivity()).create().show();
                }
            });

            if (mInfoItems.isEmpty()) {
                Info info = new Info(getWikiLink(Device.getLanguage()), getString(R.string.default_search_query));
                mInfoItems.add(info);
                mAdapter.notifyDataSetChanged();
                DatabaseFactory.getHelper().getUsefulInfoDAO().create(info);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        usefulInfo.setOnItemLongClickListener((parent, view, position, id) -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.dialog_message_delete_info);
            builder.setPositiveButton(R.string.ok, (dialog, id1) -> {
                Info info = mInfoItems.get(position);
                mInfoItems.remove(info);
                mAdapter.notifyDataSetChanged();

                try {
                    DatabaseFactory.getHelper().getUsefulInfoDAO().deleteRowByInfoDescription(info.getDescription());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
            builder.setNegativeButton(R.string.cancel, null);
            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        });

        setAppTheme(mSp);
        return v;
    }

    private String getWikiLink(String locale) {
        String link;
        switch (locale) {
            case "en_EN":
                link = Constant.INSTANCE.getWIKI_EN();
                break;

            case "de_DE":
                link = Constant.INSTANCE.getWIKI_DE();
                break;

            case "es_ES":
                link = Constant.INSTANCE.getWIKI_ES();
                break;

            case "fr_FR":
                link = Constant.INSTANCE.getWIKI_FR();
                break;

            case "it_IT":
                link = Constant.INSTANCE.getWIKI_IT();
                break;

            case "ja_JP":
                link = Constant.INSTANCE.getWIKI_JP();
                break;

            case "ru_RU":
                link = Constant.INSTANCE.getWIKI_RU();
                break;

            default:
                link = Constant.INSTANCE.getWIKI_EN();
        }
        return link;
    }

    private void openAddInfoDialog() {
        AddInfo dialog = new AddInfo();
        dialog.setOnAddInfoListener(this);
        dialog.show(getFragmentManager(), AddInfo.OPEN);
    }

    @Override
    public void onGetAddInfoText(String link, String description) {
        Info info = new Info(link, description);
        mInfoItems.add(info);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_info:
                openAddInfoDialog();
                break;

            default:
                break;
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLayout);
    }

}
