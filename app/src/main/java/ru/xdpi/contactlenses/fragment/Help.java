package ru.xdpi.contactlenses.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;
import ru.xdpi.contactlenses.model.HelpLab;

public class Help extends Fragment {
    public static final String TAG = "fragment." + Help.class.getSimpleName();
    private static final int LAYOUT = R.layout.fr_help;

    private ru.xdpi.contactlenses.model.Help mHelp;

    public Help() {
    }

    public static Help newInstance(UUID helpId) {
        Bundle args = new Bundle();
        args.putSerializable(TAG, helpId);
        Help help = new Help();
        help.setArguments(args);
        return help;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID instructionId = (UUID) getArguments().getSerializable(TAG);
        mHelp = HelpLab.get(getActivity()).getInstruction(instructionId);

        Analytics.setAppTracker(getActivity(), TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT, container, false);

        TextView description = v.findViewById(R.id.instruction_description);
        description.setText(mHelp.getDescription());

        return v;
    }

}
