package ru.xdpi.contactlenses.fragment;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.dialog.DialogController;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;
import ru.xdpi.contactlenses.utility.Device;

public class About extends Fragment {
    public static final String TAG = "fragment." + About.class.getSimpleName();
    private static final int LAYOUT = R.layout.fr_about;
    private static final int DESCRIPTION = R.id.aboutDescription;
    private static final int IC_LAUNCHER = R.id.ic_launcher;
    private static final int APP_VERSION = R.id.versionApp;

    public About() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Analytics.setAppTracker(getActivity(), TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT, container, false);

        TextView aboutDescription = v.findViewById(DESCRIPTION);
        ImageView icLauncher = v.findViewById(IC_LAUNCHER);
        TextView versionApp = v.findViewById(APP_VERSION);

        versionApp.setText(String.valueOf(getString(R.string.version) + " " + Device.getVersionName(getActivity())));

        Linkify.addLinks(aboutDescription, Linkify.EMAIL_ADDRESSES);

        icLauncher.setOnClickListener(v1 -> {
            if (Device.isOnline(getActivity())) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(Constant.INSTANCE.getFACEBOOK_LIKE()));
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            } else
                DialogController.showNoConnection(getActivity());
        });

        return v;
    }

}