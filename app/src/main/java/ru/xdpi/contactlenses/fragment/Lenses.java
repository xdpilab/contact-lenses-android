package ru.xdpi.contactlenses.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ru.xdpi.contactlenses.Constant;
import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.dialog.Eye;
import ru.xdpi.contactlenses.dialog.Note;
import ru.xdpi.contactlenses.dialog.OnEyeListener;
import ru.xdpi.contactlenses.dialog.OnNoteListener;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;
import ru.xdpi.contactlenses.service.AlarmLeft;
import ru.xdpi.contactlenses.service.AlarmRight;
import ru.xdpi.contactlenses.service.ServiceNotification;
import ru.xdpi.contactlenses.utility.Calc;
import ru.xdpi.contactlenses.utility.LOG;
import ru.xdpi.contactlenses.utility.PanelColor;
import ru.xdpi.contactlenses.utility.ToastMessage;

public class Lenses extends Fragment implements OnSharedPreferenceChangeListener, OnClickListener, OnEyeListener, OnNoteListener {
    public static final String TAG = "fragment." + Lenses.class.getSimpleName();
    private static final int LAYOUT = R.layout.fr_lenses;

    public static int LToggle = 0;
    public static int RToggle = 0;
    public static final String APP_TOAST = "system_message";
    private static final String L_TOGGLE = "l_left";
    private static final String R_TOGGLE = "r_right";
    private static final String LEFT = "left";
    private static final String RIGHT = "right";
    private static final String HOUR = "TIME_SIGNAL.hour";
    private static final String MINUTE = "TIME_SIGNAL.minute";
    private static final String LEFT_CHANGE_DAY = "left_change_day_text";
    private static final String RIGHT_CHANGE_DAY = "right_change_day_text";
    private static final String LEFT_TOGGLE = "left_toggle_state";
    private static final String RIGHT_TOGGLE = "right_toggle_state";
    private static final String CALENDAR_L = "calendarL";
    private static final String CALENDAR_R = "calendarR";
    private static final String NOTE = "note";
    private boolean mIsAppToast;
    private int mHour, mMinute;
    private int mDaysBeforeChangeLeft, mDaysBeforeChangeRight;
    private String mLeftDate, mRightDate;
    private TextView mLeftDateChange, mRightDateChange, mTextNote;
    private Button mLeftBtn, mRightBtn;
    private ToggleButton mLeftToggle, mRightToggle;
    private DateTimeFormatter mDateFormatter;
    private MutableDateTime mFutureLeftDay, mFutureRightDay;
    private LinearLayout mLensesLayout, mNotesLayout;
    private SharedPreferences mSp;
    private Editor mEd;
    private Eye mEyeDialog;

    private boolean mWasClickBCounterClick = false;
    private long mLastShowTimeBCounterClick;

    public Lenses() {
    }

    public int getDaysBeforeChangeLeft() {
        return mDaysBeforeChangeLeft;
    }

    public void setDaysBeforeChangeLeft(int daysBeforeChangeLeft) {
        mDaysBeforeChangeLeft = daysBeforeChangeLeft;
    }

    public int getDaysBeforeChangeRight() {
        return mDaysBeforeChangeRight;
    }

    public void setDaysBeforeChangeRight(int daysBeforeChangeRight) {
        mDaysBeforeChangeRight = daysBeforeChangeRight;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
        mEd = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();

        mFutureLeftDay = MutableDateTime.now();
        mFutureRightDay = MutableDateTime.now();

        mDateFormatter = DateTimeFormat.forPattern(Constant.INSTANCE.getYyyy_MM_dd());

        mEyeDialog = new Eye();
        mEyeDialog.setOnLeftRightListener(this);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && getActivity() != null)
            Analytics.setAppTracker(getActivity(), TAG);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT, container, false);

        mLensesLayout = v.findViewById(R.id.LinearLayout01);
        mNotesLayout = v.findViewById(R.id.LinearLayout02);
        mLeftDateChange = v.findViewById(R.id.textLeftChange);
        mRightDateChange = v.findViewById(R.id.textRightChange);
        mTextNote = v.findViewById(R.id.textNote);
        mLeftBtn = v.findViewById(R.id.button1);
        mRightBtn = v.findViewById(R.id.button2);
        mLeftToggle = v.findViewById(R.id.toggleButton01);
        mRightToggle = v.findViewById(R.id.toggleButton02);
        Button addNote = v.findViewById(R.id.addNoteBtn);
        Button delNote = v.findViewById(R.id.Button03);

        mLeftBtn.setText(String.valueOf(0));
        mRightBtn.setText(String.valueOf(0));

        mLeftBtn.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);
        addNote.setOnClickListener(this);
        delNote.setOnClickListener(this);

        mLeftToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                LToggle = 1;
                mLeftDateChange.setVisibility(TextView.VISIBLE);
                if (mLeftDate != null) {
                    // create Date
                    DateTime dateL = mDateFormatter.parseDateTime(mLeftDate);

                    mHour = mSp.getInt(HOUR, 12);
                    mMinute = mSp.getInt(MINUTE, 00);

                    mFutureLeftDay.setDate(dateL);
                    mFutureLeftDay.setHourOfDay(mHour);
                    mFutureLeftDay.setMinuteOfDay(mMinute);

                    mEd.putLong(CALENDAR_L, mFutureLeftDay.getMillis());
                    mEd.commit();

                    startAlarmLeft();

//                        LOG.i(TAG, "onCheckedChanged_L_HOUR:MINUTE = " + mHour + ":" + mMinute);
//                        LOG.i(TAG, "onCheckedChanged_mFutureLeftDay = " + mFutureLeftDay);
                }

            } else {
                LToggle = 0;
                mLeftDateChange.setVisibility(TextView.INVISIBLE);
                mEd.putLong(CALENDAR_L, mFutureLeftDay.getMillis());
                mEd.commit();
            }
        });

        mLeftToggle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLeftToggle.isChecked())
                    setLeftToggleTrue();
                else
                    setLeftToggleFalse();
            }
        });

        mRightToggle.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                RToggle = 1;
                mRightDateChange.setVisibility(TextView.VISIBLE);
                if (mRightDate != null) {
                    // crate Date
                    DateTime dateR = mDateFormatter.parseDateTime(mRightDate);

                    mHour = mSp.getInt(HOUR, 12);
                    mMinute = mSp.getInt(MINUTE, 0);

                    mFutureRightDay.setDate(dateR);
                    mFutureRightDay.setHourOfDay(mHour);
                    mFutureRightDay.setMinuteOfDay(mMinute);

                    mEd.putLong(CALENDAR_R, mFutureRightDay.getMillis());
                    mEd.commit();

                    startAlarmRight();

                    LOG.i(TAG, "onCheckedChanged_R_HOUR:MINUTE = " + mHour + ":" + mMinute);
                    LOG.i(TAG, "onCheckedChanged_mFutureRightDay = " + mFutureRightDay);
                }

            } else {
                RToggle = 0;
                mRightDateChange.setVisibility(TextView.INVISIBLE);
                mEd.putLong(CALENDAR_R, mFutureRightDay.getMillis());
                mEd.commit();
            }
        });

        mRightToggle.setOnClickListener(v1 -> {
            if (mRightToggle.isChecked())
                setRightToggleTrue();
            else
                setRightToggleFalse();
        });

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                // this exception can happen if press both buttons simultaneously
                try {
                    openEyeDialog(getActivity().getResources().getString(R.string.left_eye), 0, Eye.OPEN_LEFT);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.button2:
                // this exception can happen if press both buttons simultaneously
                try {
                    openEyeDialog(getActivity().getResources().getString(R.string.right_eye), 1, Eye.OPEN_RIGHT);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                break;

            case R.id.addNoteBtn:
                openNoteDialog();
                break;

            case R.id.Button03:
                if (!(mTextNote.length() == 0)) {
                    AlertDialog.Builder deleteNotes = new AlertDialog.Builder(getActivity());
                    deleteNotes.setMessage(R.string.dialog_message_delete_notes);
                    deleteNotes.setPositiveButton(R.string.ok, (dialog, id) -> {
                        mTextNote.setText("");
                        mEd.putString(Note.NOTE_SAVED, mTextNote.getText().toString());
                        mEd.apply();
                    });
                    deleteNotes.setNegativeButton(R.string.cancel, null);
                    AlertDialog dialog = deleteNotes.create();
                    dialog.show();
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mIsAppToast = mSp.getBoolean(APP_TOAST, false);
        LToggle = mSp.getInt(L_TOGGLE, 0);
        RToggle = mSp.getInt(R_TOGGLE, 0);
        load();
    }

    @Override
    public void onPause() {
        super.onPause();
        mEd.putString(LEFT_CHANGE_DAY, mLeftDateChange.getText().toString());
        mEd.putString(RIGHT_CHANGE_DAY, mRightDateChange.getText().toString());
        mEd.putString(NOTE, mTextNote.getText().toString());
        mEd.putInt(LEFT, getDaysBeforeChangeLeft());
        mEd.putInt(RIGHT, getDaysBeforeChangeRight());
        mEd.putInt(L_TOGGLE, LToggle);
        mEd.putInt(R_TOGGLE, RToggle);
        mEd.putBoolean(LEFT_TOGGLE, mLeftToggle.isChecked());
        mEd.putBoolean(RIGHT_TOGGLE, mRightToggle.isChecked());
        mEd.commit();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    @Override
    public void onGetLeftText(int inputText) {
        // important for rewriting futureLDay when left toggle already "true"
        mLeftToggle.setChecked(false);

        mDaysBeforeChangeLeft = inputText;
        mLeftBtn.setText(String.valueOf(mDaysBeforeChangeLeft));

        mHour = mSp.getInt(HOUR, 12);
        mMinute = mSp.getInt(MINUTE, 0);

        // create String for left change date
        DateTime date = new DateTime().withHourOfDay(mHour).withMinuteOfHour(mMinute)
                .withSecondOfMinute(0).plusDays(mDaysBeforeChangeLeft);
        mLeftDate = mDateFormatter.print(date);

        mLeftDateChange.setText(mLeftDate);
        mLeftToggle.setChecked(true);

//        LOG.d(TAG, "onGetLeftText_left = " + mDaysBeforeChangeLeft);
//        LOG.d(TAG, "onGetLeftText_lDate = " + mLeftDate);
    }

    @Override
    public void onGetRightText(int inputText) {
        // important for rewriting futureLDay when left toggle already "true"
        mRightToggle.setChecked(false);

        mDaysBeforeChangeRight = inputText;
        mRightBtn.setText(String.valueOf(mDaysBeforeChangeRight));

        mHour = mSp.getInt(HOUR, 12);
        mMinute = mSp.getInt(MINUTE, 0);

        // create String for left change date
        DateTime date = new DateTime().withHourOfDay(mHour).withMinuteOfHour(mMinute)
                .withSecondOfMinute(0).plusDays(mDaysBeforeChangeRight);
        mRightDate = mDateFormatter.print(date);

        mRightDateChange.setText(mRightDate);
        mRightToggle.setChecked(true);

//        LOG.i(TAG, "onGetRightText_right = " + mDaysBeforeChangeRight);
//        LOG.i(TAG, "onGetRightText_rDate = " + mRightDate);
    }

    @Override
    public void onGetNoteText(String inputText) {
        mTextNote.setText(inputText);
    }

    private void setLeftToggleTrue() {
        startAlarmLeft();
        if (mIsAppToast)
            ToastMessage.toastS(getActivity(), R.string.toast_start, true);
    }

    private void setLeftToggleFalse() {
        getActivity().stopService(new Intent(getActivity(), AlarmLeft.class));
        getActivity().stopService(new Intent(getActivity(), ServiceNotification.class));
        if (mIsAppToast)
            ToastMessage.toastS(getActivity(), R.string.toast_stop, true);
    }

    private void setRightToggleTrue() {
        startAlarmRight();
        if (mIsAppToast)
            ToastMessage.toastS(getActivity(), R.string.toast_start, true);
    }

    private void setRightToggleFalse() {
        getActivity().stopService(new Intent(getActivity(), AlarmRight.class));
        getActivity().stopService(new Intent(getActivity(), ServiceNotification.class));
        if (mIsAppToast)
            ToastMessage.toastS(getActivity(), R.string.toast_stop, true);
    }

    private void startAlarmLeft() {
        getActivity().startService(new Intent(getActivity(), AlarmLeft.class));
        getActivity().stopService(new Intent(getActivity(), ServiceNotification.class));
    }

    private void startAlarmRight() {
        getActivity().startService(new Intent(getActivity(), AlarmRight.class));
        getActivity().stopService(new Intent(getActivity(), ServiceNotification.class));
    }

    private void openEyeDialog(String eyeText, int eyeTextInt, String leftOrRight) {
        Bundle bundle = new Bundle();
        bundle.putString(Eye.EYE_TEXT, eyeText);
        bundle.putInt(Eye.EYE_TEXT_INT, eyeTextInt);
        mEyeDialog.setArguments(bundle);
        mEyeDialog.show(getFragmentManager(), leftOrRight);
    }

    private void openNoteDialog() {
        Note dialog = new Note();
        dialog.setOnNoteListener(this);
        dialog.show(getFragmentManager(), Note.OPEN);
    }

    private void loadTextOfChangeDay() {
        if (mLeftToggle.isChecked()) {
            String leftChange = mSp.getString(LEFT_CHANGE_DAY, "");
            mLeftDateChange.setText(leftChange);
            mLeftDateChange.setVisibility(TextView.VISIBLE);
        } else {
            String leftChange = mSp.getString(LEFT_CHANGE_DAY, "");
            mLeftDateChange.setText(leftChange);
            mLeftDateChange.setVisibility(TextView.INVISIBLE);
        }

        if (mRightToggle.isChecked()) {
            String rightChange = mSp.getString(RIGHT_CHANGE_DAY, "");
            mRightDateChange.setText(rightChange);
            mRightDateChange.setVisibility(TextView.VISIBLE);
        } else {
            String rightChange = mSp.getString(RIGHT_CHANGE_DAY, "");
            mRightDateChange.setText(rightChange);
            mRightDateChange.setVisibility(TextView.INVISIBLE);
        }
    }

    private void load() {
        Calc calc = new Calc();

        // load state of toggles
        boolean l = mSp.getBoolean(LEFT_TOGGLE, mLeftToggle.isChecked());
        mLeftToggle.setChecked(l);
        boolean r = mSp.getBoolean(RIGHT_TOGGLE, mRightToggle.isChecked());
        mRightToggle.setChecked(r);

        loadTextOfChangeDay();

        int leftInt = mSp.getInt(LEFT, mDaysBeforeChangeLeft);
        setDaysBeforeChangeLeft(leftInt);
        mLeftBtn.setText(String.valueOf(leftInt));

        int rightInt = mSp.getInt(RIGHT, mDaysBeforeChangeRight);
        setDaysBeforeChangeRight(rightInt);
        mRightBtn.setText(String.valueOf(rightInt));

        mFutureLeftDay.setMillis(mSp.getLong(CALENDAR_L, 0));

        if (mLeftToggle.isChecked()) {
            DateTime start = new DateTime();
            DateTime end = new DateTime(mFutureLeftDay);
            int diff = calc.doCalc(end, start);
            mLeftBtn.setText(String.valueOf(diff));
            setDaysBeforeChangeLeft(diff);
        } else {
            mLeftDate = mDateFormatter.print(new DateTime().plusDays(mDaysBeforeChangeLeft));
            mLeftDateChange.setText(mLeftDate);
        }

        mFutureRightDay.setMillis(mSp.getLong(CALENDAR_R, 0));

        if (mRightToggle.isChecked()) {
            DateTime start = new DateTime();
            DateTime end = new DateTime(mFutureRightDay);
            int diff = calc.doCalc(end, start);
            mRightBtn.setText(String.valueOf(diff));
            setDaysBeforeChangeRight(diff);
        } else {
            mRightDate = mDateFormatter.print(new DateTime().plusDays(mDaysBeforeChangeRight));
            mRightDateChange.setText(mRightDate);
        }

        String text = mSp.getString(NOTE, "");
        mTextNote.setText(text);
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLensesLayout, mNotesLayout);
    }

}
