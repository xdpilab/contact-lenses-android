package ru.xdpi.contactlenses.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;

public class MapParam extends PreferenceFragment {
    public static final String TAG = "fragment." + MapParam.class.getSimpleName();
    private static final int LAYOUT = R.xml.pref_map;

    public MapParam() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(LAYOUT);

        Analytics.setAppTracker(getActivity(), TAG);
    }
}
