package ru.xdpi.contactlenses.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.List;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.activity.MainActivity;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;
import ru.xdpi.contactlenses.database.DatabaseFactory;
import ru.xdpi.contactlenses.dialog.OnEditRecipeListener;
import ru.xdpi.contactlenses.dialog.OnRecipeListListener;
import ru.xdpi.contactlenses.dialog.RecipeEdit;
import ru.xdpi.contactlenses.dialog.RecipeList;
import ru.xdpi.contactlenses.utility.PanelColor;
import ru.xdpi.contactlenses.utility.ToastMessage;

public class Recipe extends Fragment implements OnSharedPreferenceChangeListener, OnClickListener,
        OnEditRecipeListener, OnRecipeListListener {
    public static final String TAG = "fragment." + Recipe.class.getSimpleName();
    private static final int LAYOUT = R.layout.fr_recipe;

    public static final String INDEX_RECIPE = "sp_index_recipe";
    private SharedPreferences mSp;
    private LinearLayout mLayout;
    private TextView mRecipeName, mLensesName, mDaysBeforeChange, mSphLeft, mSphRight, mCylLeft,
            mCylRight, mAxLeft, mAxRight, mBcLeft, mBcRight, mDiaLeft, mDiaRight, mAddLeft, mAddRight,
            mExtraLeft, mExtraRight;
    private int mIndex;
    private Bundle mBundle;
    private SharedPreferences.Editor mEd;

    public Recipe() {
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSp.registerOnSharedPreferenceChangeListener(this);
        mEd = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();

        mBundle = new Bundle();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            Analytics.setAppTracker(getActivity(), TAG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(LAYOUT, container, false);

        mLayout = v.findViewById(R.id.LinearLayout03);
        Button editRecipeBtn = v.findViewById(R.id.editRecipe);
        Button listRecipeBtn = v.findViewById(R.id.listRecipe);
        mRecipeName = v.findViewById(R.id.recipe_name);
        mLensesName = v.findViewById(R.id.TextView18);
        mDaysBeforeChange = v.findViewById(R.id.TextView19);
        mSphLeft = v.findViewById(R.id.TextView01);
        mSphRight = v.findViewById(R.id.TextView02);
        mCylLeft = v.findViewById(R.id.TextView555);
        mCylRight = v.findViewById(R.id.TextView07);
        mAxLeft = v.findViewById(R.id.TextView11);
        mAxRight = v.findViewById(R.id.TextView12);
        mBcLeft = v.findViewById(R.id.TextView150);
        mBcRight = v.findViewById(R.id.textView143);
        mDiaLeft = v.findViewById(R.id.textView145);
        mDiaRight = v.findViewById(R.id.textView147);
        mAddLeft = v.findViewById(R.id.TextView148);
        mAddRight = v.findViewById(R.id.textView149);
        mExtraLeft = v.findViewById(R.id.TextView151);
        mExtraRight = v.findViewById(R.id.textView152);

        editRecipeBtn.setOnClickListener(this);
        listRecipeBtn.setOnClickListener(this);

        setAppTheme(mSp);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mIndex = mSp.getInt(INDEX_RECIPE, 0);

        try {
            load();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mEd.putInt(INDEX_RECIPE, mIndex);
        mEd.apply();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        setAppTheme(sp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editRecipe:
                openEditDialog();
                break;

            case R.id.listRecipe:
                try {
                    List<ru.xdpi.contactlenses.model.Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
                    if (!recipes.isEmpty())
                        openRecipeListDialog();
                    else
                        ToastMessage.toastL(getActivity(), R.string.toast_recipe_list_empty, true);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

    private void openEditDialog() {
        FragmentManager fm = getFragmentManager();
        RecipeEdit recipeEditDialog = new RecipeEdit();
        mBundle.putInt(INDEX_RECIPE, mIndex);

        recipeEditDialog.setArguments(mBundle);
        recipeEditDialog.setOnEditActionListener(this);
        recipeEditDialog.show(fm, RecipeEdit.OPEN);
    }

    private void openRecipeListDialog() {
        FragmentManager fm = getFragmentManager();
        RecipeList recipeListDialog = new RecipeList();
        MainActivity.INDEX_RECIPE_PUBLIC = mIndex;

        recipeListDialog.setArguments(mBundle);
        recipeListDialog.setOnRecipeListListener(this);
        recipeListDialog.show(fm, RecipeList.OPEN);
    }

    @Override
    public void onGetEditRecipeText(
            String recipeName, String lenses, String days,
            String leftSPH, String rightSPH,
            String leftCYL, String rightCYL,
            String leftAX, String rightAX,
            String leftBC, String rightBC,
            String leftDIA, String rightDIA,
            String leftADD, String rightADD,
            String extra, String extra1) {
        mRecipeName.setText(recipeName);
        mLensesName.setText(lenses);
        mDaysBeforeChange.setText(days);
        mSphLeft.setText(leftSPH);
        mSphRight.setText(rightSPH);
        mCylLeft.setText(leftCYL);
        mCylRight.setText(rightCYL);
        mAxLeft.setText(leftAX);
        mAxRight.setText(rightAX);
        mBcLeft.setText(leftBC);
        mBcRight.setText(rightBC);
        mDiaLeft.setText(leftDIA);
        mDiaRight.setText(rightDIA);
        mAddLeft.setText(leftADD);
        mAddRight.setText(rightADD);
        mExtraLeft.setText(extra);
        mExtraRight.setText(extra1);

        try {
            List<ru.xdpi.contactlenses.model.Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
            mIndex = recipes.size() - 1;

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onGetRecipeList(int index) {
        if (index >= 0) {
            MainActivity.INDEX_RECIPE_PUBLIC = index;
            mIndex = index;
            try {
                List<ru.xdpi.contactlenses.model.Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
                if (!recipes.isEmpty()) {
                    mRecipeName.setText(recipes.get(index).getRecipeName());
                    mLensesName.setText(recipes.get(index).getLensesName());
                    mDaysBeforeChange.setText(recipes.get(index).getDaysBeforeChange());

                    mSphLeft.setText(recipes.get(index).getSphLeft());
                    mSphRight.setText(recipes.get(index).getSphRight());

                    mCylLeft.setText(recipes.get(index).getCylLeft());
                    mCylRight.setText(recipes.get(index).getCylRight());

                    mAxLeft.setText(recipes.get(index).getAxLeft());
                    mAxRight.setText(recipes.get(index).getAxRight());

                    mBcLeft.setText(recipes.get(index).getBcLeft());
                    mBcRight.setText(recipes.get(index).getBcRight());

                    mDiaLeft.setText(recipes.get(index).getDiaLeft());
                    mDiaRight.setText(recipes.get(index).getDiaRight());

                    mAddLeft.setText(recipes.get(index).getAddLeft());
                    mAddRight.setText(recipes.get(index).getAddRight());

                    mExtraLeft.setText(recipes.get(index).getExtraLeft());
                    mExtraRight.setText(recipes.get(index).getExtraRight());
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            mRecipeName.setText("");
            mLensesName.setText("");
            mDaysBeforeChange.setText("");

            mSphLeft.setText("");
            mSphRight.setText("");

            mCylLeft.setText("");
            mCylRight.setText("");

            mAxLeft.setText("");
            mAxRight.setText("");

            mBcLeft.setText("");
            mBcRight.setText("");

            mDiaLeft.setText("");
            mDiaRight.setText("");

            mAddLeft.setText("");
            mAddRight.setText("");

            mExtraLeft.setText("");
            mExtraRight.setText("");
        }
    }

    void load() {
        try {
            List<ru.xdpi.contactlenses.model.Recipe> recipes = DatabaseFactory.getHelper().getRecipeDAO().getAll();
            if (!recipes.isEmpty()) {
                MainActivity.INDEX_RECIPE_PUBLIC = mIndex;
                mRecipeName.setText(recipes.get(mIndex).getRecipeName());
                mLensesName.setText(recipes.get(mIndex).getLensesName());
                mDaysBeforeChange.setText(recipes.get(mIndex).getDaysBeforeChange());

                mSphLeft.setText(recipes.get(mIndex).getSphLeft());
                mSphRight.setText(recipes.get(mIndex).getSphRight());

                mCylLeft.setText(recipes.get(mIndex).getCylLeft());
                mCylRight.setText(recipes.get(mIndex).getCylRight());

                mAxLeft.setText(recipes.get(mIndex).getAxLeft());
                mAxRight.setText(recipes.get(mIndex).getAxRight());

                mBcLeft.setText(recipes.get(mIndex).getBcLeft());
                mBcRight.setText(recipes.get(mIndex).getBcRight());

                mDiaLeft.setText(recipes.get(mIndex).getDiaLeft());
                mDiaRight.setText(recipes.get(mIndex).getDiaRight());

                mAddLeft.setText(recipes.get(mIndex).getAddLeft());
                mAddRight.setText(recipes.get(mIndex).getAddRight());

                mExtraLeft.setText(recipes.get(mIndex).getExtraLeft());
                mExtraRight.setText(recipes.get(mIndex).getExtraRight());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setAppTheme(SharedPreferences sp) {
        String key = sp.getString(PanelColor.PANEL_COLOR, "0");
        new PanelColor().setBgFragment(sp, key, mLayout);
    }

}
