package ru.xdpi.contactlenses.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.googleservice.analytics.Analytics;

public class Settings extends PreferenceFragment {
    public static final String TAG = "fragment." + Settings.class.getSimpleName();
    private static final int LAYOUT = R.xml.pref_main;

    public Settings() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(LAYOUT);

        Analytics.setAppTracker(getActivity(), TAG);
    }

}
