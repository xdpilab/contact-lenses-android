package ru.xdpi.contactlenses.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;

import org.joda.time.DateTime;

import ru.xdpi.contactlenses.receiver.ReceiverLeft;

public class AlarmLeft extends Service {
    private static final String TAG = "service." + AlarmLeft.class.getSimpleName();
    private static final String HOUR = "TIME_SIGNAL.hour";
    private static final String MINUTE = "TIME_SIGNAL.minute";
    private static final String CALENDAR_L = "calendarL";
    private AlarmManager mAlarmManager;
    private DateTime mDateTimeEnd;

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        int hour = sp.getInt(HOUR, 12);
        int minute = sp.getInt(MINUTE, 00);

        mDateTimeEnd = new DateTime(sp.getLong(CALENDAR_L, 0)).withHourOfDay(hour).withMinuteOfHour(minute).withSecondOfMinute(00);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startService();
        stopSelf();
        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startService() {
        Intent i = new Intent(AlarmLeft.this, ReceiverLeft.class);
        PendingIntent pi = PendingIntent.getBroadcast(AlarmLeft.this, 0, i, 0);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, mDateTimeEnd.getMillis(), pi);
    }

}