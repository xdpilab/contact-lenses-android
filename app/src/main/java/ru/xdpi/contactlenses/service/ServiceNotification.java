package ru.xdpi.contactlenses.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import ru.xdpi.contactlenses.R;
import ru.xdpi.contactlenses.activity.MainActivity;
import ru.xdpi.contactlenses.dialog.PrefNotifyText;

public class ServiceNotification extends Service implements OnSharedPreferenceChangeListener {
    private static final String TAG = "service." + ServiceNotification.class.getSimpleName();
    private static final String SOUND_REMINDER = "sound_reminder";
    private static final String VIBRATION = "vibration";
    private static final String LED = "led";
    private static final int NOTIFY_ID = 101;
    private NotificationManager mNotificationManager;
    private SharedPreferences mSp;
    private String mNotifyText;

    @Override
    public void onCreate() {
        super.onCreate();
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        mSp = PreferenceManager.getDefaultSharedPreferences(this);
        mSp.registerOnSharedPreferenceChangeListener(this);

        if ("".equals(mSp.getString(PrefNotifyText.NOTIFY_TEXT, "")))
            mNotifyText = getResources().getString(R.string.notify_title);
        else
            mNotifyText = mSp.getString(PrefNotifyText.NOTIFY_TEXT, "");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        sendNotification();
        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    private void sendNotification() {
        Context context = getApplicationContext();
        Resources res = context.getResources();

        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "default");
        builder.setContentIntent(pi)
                .setSmallIcon(R.drawable.ic_notification).setAutoCancel(true)
                .setContentTitle(mNotifyText)
                .setContentText(res.getString(R.string.notify_message));

        Notification n = builder.build();

        if (mSp.getBoolean(SOUND_REMINDER, true))
            n.defaults |= Notification.DEFAULT_SOUND;

        if (mSp.getBoolean(VIBRATION, true))
            n.defaults |= Notification.DEFAULT_VIBRATE;

        if (mSp.getBoolean(LED, true)) {
            n.defaults |= Notification.DEFAULT_LIGHTS;
            n.flags |= Notification.FLAG_SHOW_LIGHTS;
        }

        mNotificationManager.notify(NOTIFY_ID, n);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }
}
