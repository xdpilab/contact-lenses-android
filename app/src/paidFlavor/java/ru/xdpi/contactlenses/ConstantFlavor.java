package ru.xdpi.contactlenses;

public class ConstantFlavor {
    public static final BuildType BUILD_TYPE = BuildType.PAID;
    public static final String CONTACT_LENSES = "market://details?id=ru.xdpi.contactlensespro";
    public static final String CONTACT_LENSES_SHARE = "https://market.android.com/details?id=ru.xdpi.contactlensespro";
    public static final String URL_CONTACT_LENSES = "https://play.google.com/store/apps/details?id=ru.xdpi.contactlensespro";
    public static final int LIMIT_DAYS = 365;
    public static final String PROPERTY_ID = "UA-49516762-2";
}
